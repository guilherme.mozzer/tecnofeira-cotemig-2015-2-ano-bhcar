﻿namespace BhCar
{
    partial class frmFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFornecedor));
            this.pictureBoxCabecalho = new System.Windows.Forms.PictureBox();
            this.lblObrigatorio = new System.Windows.Forms.Label();
            this.dtgfornecedor = new System.Windows.Forms.DataGridView();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.txtMarca = new System.Windows.Forms.TextBox();
            this.lblMarca = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.lblTelefone1 = new System.Windows.Forms.Label();
            this.bntCadastrar = new System.Windows.Forms.PictureBox();
            this.bntExcluir = new System.Windows.Forms.PictureBox();
            this.bntAlterar = new System.Windows.Forms.PictureBox();
            this.bntPesquisar = new System.Windows.Forms.PictureBox();
            this.bntVoltar = new System.Windows.Forms.PictureBox();
            this.txtTelefone1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCabecalho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgfornecedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCadastrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntExcluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAlterar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntPesquisar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVoltar)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxCabecalho
            // 
            this.pictureBoxCabecalho.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCabecalho.Image")));
            this.pictureBoxCabecalho.Location = new System.Drawing.Point(0, 1);
            this.pictureBoxCabecalho.Name = "pictureBoxCabecalho";
            this.pictureBoxCabecalho.Size = new System.Drawing.Size(645, 82);
            this.pictureBoxCabecalho.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCabecalho.TabIndex = 2;
            this.pictureBoxCabecalho.TabStop = false;
            // 
            // lblObrigatorio
            // 
            this.lblObrigatorio.AutoSize = true;
            this.lblObrigatorio.Location = new System.Drawing.Point(519, 187);
            this.lblObrigatorio.Name = "lblObrigatorio";
            this.lblObrigatorio.Size = new System.Drawing.Size(111, 13);
            this.lblObrigatorio.TabIndex = 179;
            this.lblObrigatorio.Text = "* Campos Obrigatórios";
            // 
            // dtgfornecedor
            // 
            this.dtgfornecedor.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dtgfornecedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgfornecedor.Location = new System.Drawing.Point(12, 203);
            this.dtgfornecedor.Name = "dtgfornecedor";
            this.dtgfornecedor.Size = new System.Drawing.Size(618, 210);
            this.dtgfornecedor.TabIndex = 5;
            this.dtgfornecedor.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgfornecedor_CellContentClick);
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(61, 111);
            this.txtNome.MaxLength = 100;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(569, 20);
            this.txtNome.TabIndex = 2;
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(13, 114);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(45, 13);
            this.lblNome.TabIndex = 177;
            this.lblNome.Text = "* Nome:";
            // 
            // txtMarca
            // 
            this.txtMarca.Location = new System.Drawing.Point(375, 88);
            this.txtMarca.MaxLength = 45;
            this.txtMarca.Name = "txtMarca";
            this.txtMarca.Size = new System.Drawing.Size(255, 20);
            this.txtMarca.TabIndex = 1;
            // 
            // lblMarca
            // 
            this.lblMarca.AutoSize = true;
            this.lblMarca.Location = new System.Drawing.Point(321, 91);
            this.lblMarca.Name = "lblMarca";
            this.lblMarca.Size = new System.Drawing.Size(47, 13);
            this.lblMarca.TabIndex = 176;
            this.lblMarca.Text = "* Marca:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(61, 137);
            this.txtEmail.MaxLength = 80;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(569, 20);
            this.txtEmail.TabIndex = 3;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(13, 140);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(45, 13);
            this.lblEmail.TabIndex = 175;
            this.lblEmail.Text = "* E-mail:";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Cursor = System.Windows.Forms.Cursors.No;
            this.txtCodigo.Enabled = false;
            this.txtCodigo.Location = new System.Drawing.Point(61, 88);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(222, 20);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.TabStop = false;
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(13, 91);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(43, 13);
            this.lblCodigo.TabIndex = 174;
            this.lblCodigo.Text = "Código:";
            // 
            // lblTelefone1
            // 
            this.lblTelefone1.AutoSize = true;
            this.lblTelefone1.Location = new System.Drawing.Point(13, 166);
            this.lblTelefone1.Name = "lblTelefone1";
            this.lblTelefone1.Size = new System.Drawing.Size(59, 13);
            this.lblTelefone1.TabIndex = 169;
            this.lblTelefone1.Text = "* Telefone:";
            // 
            // bntCadastrar
            // 
            this.bntCadastrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntCadastrar.Image = ((System.Drawing.Image)(resources.GetObject("bntCadastrar.Image")));
            this.bntCadastrar.Location = new System.Drawing.Point(12, 419);
            this.bntCadastrar.Name = "bntCadastrar";
            this.bntCadastrar.Size = new System.Drawing.Size(150, 40);
            this.bntCadastrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntCadastrar.TabIndex = 180;
            this.bntCadastrar.TabStop = false;
            this.bntCadastrar.Click += new System.EventHandler(this.bntCadastrar_Click);
            // 
            // bntExcluir
            // 
            this.bntExcluir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntExcluir.Image = ((System.Drawing.Image)(resources.GetObject("bntExcluir.Image")));
            this.bntExcluir.Location = new System.Drawing.Point(480, 419);
            this.bntExcluir.Name = "bntExcluir";
            this.bntExcluir.Size = new System.Drawing.Size(150, 40);
            this.bntExcluir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntExcluir.TabIndex = 181;
            this.bntExcluir.TabStop = false;
            this.bntExcluir.Click += new System.EventHandler(this.bntExcluir_Click);
            // 
            // bntAlterar
            // 
            this.bntAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntAlterar.Image = ((System.Drawing.Image)(resources.GetObject("bntAlterar.Image")));
            this.bntAlterar.Location = new System.Drawing.Point(324, 419);
            this.bntAlterar.Name = "bntAlterar";
            this.bntAlterar.Size = new System.Drawing.Size(150, 40);
            this.bntAlterar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntAlterar.TabIndex = 182;
            this.bntAlterar.TabStop = false;
            this.bntAlterar.Click += new System.EventHandler(this.bntAlterar_Click);
            // 
            // bntPesquisar
            // 
            this.bntPesquisar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("bntPesquisar.Image")));
            this.bntPesquisar.Location = new System.Drawing.Point(168, 419);
            this.bntPesquisar.Name = "bntPesquisar";
            this.bntPesquisar.Size = new System.Drawing.Size(150, 40);
            this.bntPesquisar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntPesquisar.TabIndex = 183;
            this.bntPesquisar.TabStop = false;
            this.bntPesquisar.Click += new System.EventHandler(this.bntPesquisar_Click);
            // 
            // bntVoltar
            // 
            this.bntVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntVoltar.Image = ((System.Drawing.Image)(resources.GetObject("bntVoltar.Image")));
            this.bntVoltar.Location = new System.Drawing.Point(480, 465);
            this.bntVoltar.Name = "bntVoltar";
            this.bntVoltar.Size = new System.Drawing.Size(150, 40);
            this.bntVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntVoltar.TabIndex = 184;
            this.bntVoltar.TabStop = false;
            this.bntVoltar.Click += new System.EventHandler(this.bntVoltar_Click);
            // 
            // txtTelefone1
            // 
            this.txtTelefone1.Location = new System.Drawing.Point(78, 163);
            this.txtTelefone1.MaxLength = 13;
            this.txtTelefone1.Name = "txtTelefone1";
            this.txtTelefone1.Size = new System.Drawing.Size(205, 20);
            this.txtTelefone1.TabIndex = 4;
            this.txtTelefone1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefone1_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(289, 166);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 186;
            this.label1.Text = "EX: (XX)00000-0000";
            // 
            // frmFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 521);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTelefone1);
            this.Controls.Add(this.bntVoltar);
            this.Controls.Add(this.bntPesquisar);
            this.Controls.Add(this.bntAlterar);
            this.Controls.Add(this.bntExcluir);
            this.Controls.Add(this.bntCadastrar);
            this.Controls.Add(this.lblObrigatorio);
            this.Controls.Add(this.dtgfornecedor);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.txtMarca);
            this.Controls.Add(this.lblMarca);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.lblTelefone1);
            this.Controls.Add(this.pictureBoxCabecalho);
            this.MaximizeBox = false;
            this.Name = "frmFornecedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fornecedor";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCabecalho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgfornecedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCadastrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntExcluir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAlterar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntPesquisar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVoltar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxCabecalho;
        private System.Windows.Forms.Label lblObrigatorio;
        private System.Windows.Forms.DataGridView dtgfornecedor;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.TextBox txtMarca;
        private System.Windows.Forms.Label lblMarca;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label lblTelefone1;
        private System.Windows.Forms.PictureBox bntCadastrar;
        private System.Windows.Forms.PictureBox bntExcluir;
        private System.Windows.Forms.PictureBox bntAlterar;
        private System.Windows.Forms.PictureBox bntPesquisar;
        private System.Windows.Forms.PictureBox bntVoltar;
        private System.Windows.Forms.TextBox txtTelefone1;
        private System.Windows.Forms.Label label1;
    }
}