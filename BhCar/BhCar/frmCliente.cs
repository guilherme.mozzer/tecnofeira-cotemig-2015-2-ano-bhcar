﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace BhCar
{
    public partial class frmCliente : Form
    {
        public frmCliente()
        {
            InitializeComponent();
        }
        conexaobd bd = new conexaobd();

        private void txtCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                switch (txtCpf.TextLength)
                {
                    case 3:
                        txtCpf.Text = txtCpf.Text + ".";
                        txtCpf.SelectionStart = 4;
                        break;
                    case 7:
                        txtCpf.Text = txtCpf.Text + ".";
                        txtCpf.SelectionStart = 8;
                        break;
                    case 11:
                        txtCpf.Text = txtCpf.Text + "-";
                        txtCpf.SelectionStart = 12;
                        break;

                }
            }
        }

        private void txtRg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                switch (txtRg.TextLength)
                {
                    case 2:
                        txtRg.Text = txtRg.Text + "-";
                        txtRg.SelectionStart = 3;
                        break;
                    case 5:
                        txtRg.Text = txtRg.Text + ".";
                        txtRg.SelectionStart = 6;
                        break;
                    case 9:
                        txtRg.Text = txtRg.Text + ".";
                        txtRg.SelectionStart = 10;
                        break;
                }
            }
        }

        private void txtCep_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                switch (txtCep.TextLength)
                {
                    case 5:
                        txtCep.Text = txtCep.Text + "-";
                        txtCep.SelectionStart = 6;
                        break;
                }
            }
        }

        private void txtTelefone1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                switch (txtTelefone1.TextLength)
                {
                    case 0:
                        txtTelefone1.Text = "(";
                        txtTelefone1.SelectionStart = 2;
                        break;
                    case 3:
                        txtTelefone1.Text = txtTelefone1.Text + ")";
                        txtTelefone1.SelectionStart = 5;
                        break;
                }
            }
        }

        private void bntCadastrar_Click(object sender, EventArgs e)
        {
                string Valor = txtCpf.Text;
                if(conexaobd.Verificarcpf(Valor))
                {
                if (txtNome.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Nome", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNome.Focus();
                }
                else if (rbnM.Checked == false && rbnF.Checked == false)
                {
                    MessageBox.Show("O Campo Sexo deve ser marcado!", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    rbnM.Focus();
                }
                else if ((!txtEmail.Text.Contains("@") || !txtEmail.Text.Contains(".")) || txtEmail.Text.Length == 0)
                {
                    MessageBox.Show("O Campo Email é Inválido!", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtEmail.Focus();
                }
                else if (txtRg.Text.Length != 13)
                {
                    MessageBox.Show("O Campo RG é Inválido", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtRg.Focus();
                }
                else if (txtComrenda.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Comprovante de Renda", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtComrenda.Focus();
                }
                else if (txtLogradouro.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Logradouro", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtLogradouro.Focus();
                }
                else if (txtNumero.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Número", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNumero.Focus();
                }
                else if (txtUf.Text.Length != 2)
                {
                    MessageBox.Show("Preencha o Campo UF", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtUf.Focus();
                }
                else if (txtCidade.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Cidade", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCidade.Focus();
                }
                else if (txtBairro.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Bairro", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtBairro.Focus();
                }
                else if (txtComend.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Comprovante de Endereço", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtComend.Focus();
                }
                else if (txtTelefone1.Text.Length != 12 || txtTelefone1.Text.Length != 13)
                {
                    MessageBox.Show("O Campo Telefone é Inválido", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTelefone1.Focus();
                }
                else
                {
                    txtId.Clear();
                    string inserir;
                    string sex = rbnM.Checked ? "M" : "F";
                    inserir = "insert into cliente (codigo, nome, cpf, sexo, rg, email, logradouro, numero, complemento, bairro, cidade, uf, cep, comprovante_de_renda, comprovante_de_endereco, telefone) ";
                    inserir += "values (null, '" + txtNome.Text + "', '" + txtCpf.Text + "', '" + sex + "', '" + txtRg.Text + "', '" + txtEmail.Text + "', '" + txtLogradouro.Text + "', " + txtNumero.Text + ", '" + txtComplemento.Text + "', '" + txtBairro.Text + "', '" + txtCidade.Text + "', '" + txtUf.Text + "', '" + txtCep.Text + "', '" + txtComrenda.Text + "', '" + txtComend.Text + "', '" + txtTelefone1.Text + "')";

                    bd.executarcomandos(inserir);
                    MessageBox.Show("Cadastrado Realizado Com Sucesso!");
                    txtNome.Clear();
                    txtCpf.Clear();
                    txtRg.Clear();
                    txtEmail.Clear();
                    txtLogradouro.Clear();
                    txtNumero.Clear();
                    txtComplemento.Clear();
                    txtBairro.Clear();
                    txtCidade.Clear();
                    txtUf.Clear();
                    txtCep.Clear();
                    txtComrenda.Clear();
                    txtComend.Clear();
                    txtTelefone1.Clear();
                    txtNome.Focus();
                }
            }
            else
            {
                MessageBox.Show("O Campo CPF é Inválido", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCpf.Focus();
            }
           
        }

        private void dtgCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtId.Text = dtgCliente.Rows[e.RowIndex].Cells["codigo"].Value.ToString();
            txtNome.Text = dtgCliente.Rows[e.RowIndex].Cells["nome"].Value.ToString();
            txtCpf.Text = dtgCliente.Rows[e.RowIndex].Cells["cpf"].Value.ToString();
            txtRg.Text = dtgCliente.Rows[e.RowIndex].Cells["rg"].Value.ToString();
            txtEmail.Text = dtgCliente.Rows[e.RowIndex].Cells["email"].Value.ToString();
            txtLogradouro.Text = dtgCliente.Rows[e.RowIndex].Cells["logradouro"].Value.ToString();
            txtNumero.Text = dtgCliente.Rows[e.RowIndex].Cells["numero"].Value.ToString();
            txtComplemento.Text = dtgCliente.Rows[e.RowIndex].Cells["complemento"].Value.ToString();
            txtBairro.Text = dtgCliente.Rows[e.RowIndex].Cells["bairro"].Value.ToString();
            txtCidade.Text = dtgCliente.Rows[e.RowIndex].Cells["cidade"].Value.ToString();
            txtUf.Text = dtgCliente.Rows[e.RowIndex].Cells["uf"].Value.ToString();
            txtCep.Text = dtgCliente.Rows[e.RowIndex].Cells["cep"].Value.ToString();
            txtComrenda.Text = dtgCliente.Rows[e.RowIndex].Cells["comprovante_de_renda"].Value.ToString();
            txtComend.Text = dtgCliente.Rows[e.RowIndex].Cells["comprovante_de_endereco"].Value.ToString();
            txtTelefone1.Text = dtgCliente.Rows[e.RowIndex].Cells["telefone"].Value.ToString();
        }

        private void bntPesquisar_Click(object sender, EventArgs e)
        {
            string sel = "SELECT * FROM cliente order by nome";
            DataTable dt = bd.executarconsulta(sel);
            dtgCliente.DataSource = dt;
        }

        private void bntAlterar_Click(object sender, EventArgs e)
        {
                if (txtCpf.Text.Length != 14)
                {
                    MessageBox.Show("O Campo CPF é Inválido", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCpf.Focus();
                }
                else if (txtNome.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Nome", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNome.Focus();
                }
                else if (rbnM.Checked == false && rbnF.Checked == false)
                {
                    MessageBox.Show("O Campo Sexo deve ser marcado!", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    rbnM.Focus();
                }
                else if ((!txtEmail.Text.Contains("@") || !txtEmail.Text.Contains(".")) || txtEmail.Text.Length == 0)
                {
                    MessageBox.Show("O Campo Email é Inválido!", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtEmail.Focus();
                }
                else if (txtRg.Text.Length != 13)
                {
                    MessageBox.Show("O Campo RG é Inválido", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtRg.Focus();
                }
                else if (txtComrenda.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Comprovante de Renda", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtComrenda.Focus();
                }
                else if (txtLogradouro.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Logradouro", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtLogradouro.Focus();
                }
                else if (txtNumero.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Número", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtNumero.Focus();
                }
                else if (txtUf.Text.Length != 2)
                {
                    MessageBox.Show("Preencha o Campo UF", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtUf.Focus();
                }
                else if (txtCidade.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Cidade", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCidade.Focus();
                }
                else if (txtBairro.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Bairro", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtBairro.Focus();
                }
                else if (txtComend.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Comprovante de Endereço", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtComend.Focus();
                }
                else if (txtTelefone1.Text.Length != 13)
                {
                    MessageBox.Show("O Campo Telefone é Inválido", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtTelefone1.Focus();
                }
                else
                {
                    string alterar;
                    string sex = rbnM.Checked ? "M" : "F";
                    alterar = "update cliente set nome ='" + txtNome.Text + "', cpf ='" + txtCpf.Text + "', sexo ='" + sex + "', rg ='" + txtRg.Text + "', email ='" + txtEmail.Text + "', logradouro ='" + txtLogradouro.Text + "', numero =" + txtNumero.Text + ", complemento ='" + txtComplemento.Text + "', bairro ='" + txtBairro.Text + "', cidade ='" + txtCidade.Text + "', uf ='" + txtUf.Text + "', cep ='" + txtCep.Text + "', comprovante_de_renda ='" + txtComrenda.Text + "', comprovante_de_endereco ='" + txtComend.Text + "', telefone = '" + txtTelefone1.Text + "' WHERE codigo = " + long.Parse(txtId.Text);
                    bd.executarcomandos(alterar);
                    MessageBox.Show("Cadastro Alterado Com Sucesso!");
                    txtNome.Clear();
                    txtCpf.Clear();
                    txtRg.Clear();
                    txtEmail.Clear();
                    txtLogradouro.Clear();
                    txtNumero.Clear();
                    txtComplemento.Clear();
                    txtBairro.Clear();
                    txtCidade.Clear();
                    txtUf.Clear();
                    txtCep.Clear();
                    txtComrenda.Clear();
                    txtComend.Clear();
                    txtTelefone1.Clear();
                    txtNome.Focus();
                }
           
        }

        private void bntVoltar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bntExcluir_Click(object sender, EventArgs e)
        {
                if (txtId.Text == "")
                {
                    MessageBox.Show("Selecione o Campo", "Confirmação",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    string sel = "SELECT * FROM cliente order by nome";
                    DataTable dt = bd.executarconsulta(sel);
                    dtgCliente.DataSource = dt;
                }
                else if (MessageBox.Show("Tem certeza que deseja Excluir o cadastro?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string excluir;
                    excluir = "delete from cliente where codigo = '" + int.Parse(txtId.Text) + "'";
                    bd.executarcomandos(excluir);
                    MessageBox.Show("Cadastro Excluido Com Sucesso!");
                    txtId.Clear();
                    txtNome.Clear();
                    txtCpf.Clear();
                    txtRg.Clear();
                    txtEmail.Clear();
                    txtLogradouro.Clear();
                    txtNumero.Clear();
                    txtComplemento.Clear();
                    txtBairro.Clear();
                    txtCidade.Clear();
                    txtUf.Clear();
                    txtCep.Clear();
                    txtComrenda.Clear();
                    txtComend.Clear();
                    txtNome.Focus();
                }
        }

        private void frmCliente_Load(object sender, EventArgs e)
        {
            string query = "SELECT login.codigo, login.usuario, login.senha, nivel.descricao FROM login JOIN nivel ON nivel.codigo = login.nivel_codigo  WHERE login.usuario = '" + dadosdelogin.Usuario + "' AND nivel.descricao = @nivel";
            conexaobd conexao = new conexaobd();
            conexao.ConectarBD();

            MySqlCommand cmd = new MySqlCommand(query, conexao.con);

            cmd.Parameters.AddWithValue("@nivel", "Funcionário");

            MySqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                bntAlterar.Visible = false;
                bntExcluir.Visible = false;
                bntVoltar.Location = new Point(499, 589);

            }
            else if (dadosdelogin.Nivel == "Gerente")
            {
                bntExcluir.Visible = false;
                bntVoltar.Location = new Point(499, 589);
            }
        }
    }
}