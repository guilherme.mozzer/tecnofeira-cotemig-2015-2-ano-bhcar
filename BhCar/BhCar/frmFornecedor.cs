﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BhCar
{
    public partial class frmFornecedor : Form
    {
        public frmFornecedor()
        {
            InitializeComponent();
        }
        conexaobd bd = new conexaobd();
        private void bntCadastrar_Click(object sender, EventArgs e)
        {
            string inserir;
            if (txtNome.Text == "")
            {
                MessageBox.Show("Preencha o Campo Nome", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNome.Focus();
            }
            else if ((!txtEmail.Text.Contains("@") || !txtEmail.Text.Contains(".")) || txtEmail.Text.Length == 0)
            {
                MessageBox.Show("O Campo Email é Inválido!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.Focus();
            }
            else if (txtTelefone1.Text.Length != 12 || txtTelefone1.Text.Length != 13)
            {
                MessageBox.Show("O Campo Telefone é Inválido", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTelefone1.Focus();
            }
            else if (txtMarca.Text == "")
            {
                MessageBox.Show("Preencha o Campo Marca", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMarca.Focus();
            }
            else
            {
                txtCodigo.Clear();
                inserir = "insert into fornecedor (codigo, nome, marca, email, tel) values (null, '" + txtNome.Text + "', '" + txtMarca.Text + "', '" + txtEmail.Text + "', '" + txtTelefone1.Text + "')";

                bd.executarcomandos(inserir);
                MessageBox.Show("Cadastro Realizado com sucesso!");
                txtCodigo.Clear();
                txtMarca.Clear();
                txtNome.Clear();
                txtEmail.Clear();
                txtTelefone1.Clear();
            }

        }

        private void bntPesquisar_Click(object sender, EventArgs e)
        {
            string sel = "SELECT *FROM fornecedor order by nome";
            DataTable dt = bd.executarconsulta(sel);
            dtgfornecedor.DataSource = dt;
        }

        private void txtTelefone1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                switch (txtTelefone1.TextLength)
                {
                    case 0: txtTelefone1.Text = "(";
                        txtTelefone1.SelectionStart = 2;
                        break;
                    case 3:
                        txtTelefone1.Text = txtTelefone1.Text + ")";
                        txtTelefone1.SelectionStart = 4;
                        break;
                }
            }
        }

        private void bntAlterar_Click(object sender, EventArgs e)
        {
            if (txtNome.Text == "")
            {
                MessageBox.Show("Preencha o Campo Nome", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNome.Focus();
            }
            else if ((!txtEmail.Text.Contains("@") || !txtEmail.Text.Contains(".")) || txtEmail.Text.Length == 0)
            {
                MessageBox.Show("O Campo Email é Inválido!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.Focus();
            }
            else if (txtTelefone1.Text.Length != 13)
            {
                MessageBox.Show("O Campo Telefone é Inválido", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTelefone1.Focus();
            }
            else if (txtMarca.Text == "")
            {
                MessageBox.Show("Preencha o Campo Marca", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMarca.Focus();
            }
            else
            {
                string alterar;
                alterar = "update fornecedor set  nome = '" + txtNome.Text + "', marca ='" + txtMarca.Text + "', email = '" + txtEmail.Text + "', tel ='" + txtTelefone1.Text + "'where codigo = " + long.Parse(txtCodigo.Text) ;
                bd.executarcomandos(alterar);
                MessageBox.Show("Cadastro Alterado com sucesso!");
                txtCodigo.Clear();
                txtMarca.Clear();
                txtNome.Clear();
                txtEmail.Clear();
                txtTelefone1.Clear();
            }
        }

        private void dtgfornecedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtCodigo.Text = dtgfornecedor.Rows[e.RowIndex].Cells["codigo"].Value.ToString();
            txtNome.Text = dtgfornecedor.Rows[e.RowIndex].Cells["nome"].Value.ToString();
            txtMarca.Text = dtgfornecedor.Rows[e.RowIndex].Cells["marca"].Value.ToString();
            txtEmail.Text = dtgfornecedor.Rows[e.RowIndex].Cells["email"].Value.ToString();
            txtTelefone1.Text = dtgfornecedor.Rows[e.RowIndex].Cells["tel"].Value.ToString();
        }

        private void bntExcluir_Click(object sender, EventArgs e)
        {
            string excluir;
            if (txtCodigo.Text == "")
            {
                MessageBox.Show("Selecione o Campo", "Aviso",
             MessageBoxButtons.OK, MessageBoxIcon.Warning);
                string sel = "SELECT *FROM fornecedor order by nome";
                DataTable dt = bd.executarconsulta(sel);
                dtgfornecedor.DataSource = dt;
            }
            else if (MessageBox.Show("Tem certeza que deseja Excluir o cadastro?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                    excluir = "delete from fornecedor where codigo = '" + long.Parse(txtCodigo.Text) + "'";
                    bd.executarcomandos(excluir);
                    MessageBox.Show("Cadastro Excluido com sucesso!");
                    txtCodigo.Clear();
                    txtMarca.Clear();
                    txtNome.Clear();
                    txtEmail.Clear();
                    txtTelefone1.Clear();
            }
            
            
        }

        private void bntVoltar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

