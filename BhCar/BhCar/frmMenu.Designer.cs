﻿namespace BhCar
{
    partial class frmMenu
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.PctCabecalho = new System.Windows.Forms.PictureBox();
            this.bntAutomovel = new System.Windows.Forms.PictureBox();
            this.bntCliente = new System.Windows.Forms.PictureBox();
            this.bntFornecedor = new System.Windows.Forms.PictureBox();
            this.bntFormapagamento = new System.Windows.Forms.PictureBox();
            this.bntCompra = new System.Windows.Forms.PictureBox();
            this.bntVendedor = new System.Windows.Forms.PictureBox();
            this.bntEstoque = new System.Windows.Forms.PictureBox();
            this.bntSair = new System.Windows.Forms.PictureBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.PctCabecalho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAutomovel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntFornecedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntFormapagamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCompra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVendedor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntEstoque)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntSair)).BeginInit();
            this.SuspendLayout();
            // 
            // PctCabecalho
            // 
            this.PctCabecalho.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PctCabecalho.Cursor = System.Windows.Forms.Cursors.Default;
            this.PctCabecalho.Enabled = false;
            this.PctCabecalho.Image = ((System.Drawing.Image)(resources.GetObject("PctCabecalho.Image")));
            this.PctCabecalho.ImageLocation = "";
            this.PctCabecalho.Location = new System.Drawing.Point(0, 0);
            this.PctCabecalho.Name = "PctCabecalho";
            this.PctCabecalho.Size = new System.Drawing.Size(600, 245);
            this.PctCabecalho.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PctCabecalho.TabIndex = 0;
            this.PctCabecalho.TabStop = false;
            // 
            // bntAutomovel
            // 
            this.bntAutomovel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bntAutomovel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntAutomovel.Image = ((System.Drawing.Image)(resources.GetObject("bntAutomovel.Image")));
            this.bntAutomovel.Location = new System.Drawing.Point(0, 259);
            this.bntAutomovel.Name = "bntAutomovel";
            this.bntAutomovel.Size = new System.Drawing.Size(241, 50);
            this.bntAutomovel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntAutomovel.TabIndex = 1;
            this.bntAutomovel.TabStop = false;
            this.bntAutomovel.Click += new System.EventHandler(this.bntAutomovel_Click);
            // 
            // bntCliente
            // 
            this.bntCliente.AccessibleDescription = "";
            this.bntCliente.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bntCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntCliente.Image = ((System.Drawing.Image)(resources.GetObject("bntCliente.Image")));
            this.bntCliente.Location = new System.Drawing.Point(0, 315);
            this.bntCliente.Name = "bntCliente";
            this.bntCliente.Size = new System.Drawing.Size(241, 50);
            this.bntCliente.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntCliente.TabIndex = 2;
            this.bntCliente.TabStop = false;
            this.bntCliente.Click += new System.EventHandler(this.bntCliente_Click);
            // 
            // bntFornecedor
            // 
            this.bntFornecedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bntFornecedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntFornecedor.Image = ((System.Drawing.Image)(resources.GetObject("bntFornecedor.Image")));
            this.bntFornecedor.Location = new System.Drawing.Point(0, 427);
            this.bntFornecedor.Name = "bntFornecedor";
            this.bntFornecedor.Size = new System.Drawing.Size(241, 50);
            this.bntFornecedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntFornecedor.TabIndex = 2;
            this.bntFornecedor.TabStop = false;
            this.bntFornecedor.Click += new System.EventHandler(this.bntFornecedor_Click);
            // 
            // bntFormapagamento
            // 
            this.bntFormapagamento.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bntFormapagamento.BackColor = System.Drawing.SystemColors.Control;
            this.bntFormapagamento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntFormapagamento.Image = ((System.Drawing.Image)(resources.GetObject("bntFormapagamento.Image")));
            this.bntFormapagamento.Location = new System.Drawing.Point(0, 371);
            this.bntFormapagamento.Name = "bntFormapagamento";
            this.bntFormapagamento.Size = new System.Drawing.Size(241, 50);
            this.bntFormapagamento.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntFormapagamento.TabIndex = 3;
            this.bntFormapagamento.TabStop = false;
            this.bntFormapagamento.Click += new System.EventHandler(this.bntFormapagamento_Click);
            // 
            // bntCompra
            // 
            this.bntCompra.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntCompra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntCompra.Image = ((System.Drawing.Image)(resources.GetObject("bntCompra.Image")));
            this.bntCompra.Location = new System.Drawing.Point(359, 259);
            this.bntCompra.Name = "bntCompra";
            this.bntCompra.Size = new System.Drawing.Size(270, 50);
            this.bntCompra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntCompra.TabIndex = 4;
            this.bntCompra.TabStop = false;
            this.bntCompra.Click += new System.EventHandler(this.bntCompra_Click);
            // 
            // bntVendedor
            // 
            this.bntVendedor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntVendedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntVendedor.Image = ((System.Drawing.Image)(resources.GetObject("bntVendedor.Image")));
            this.bntVendedor.Location = new System.Drawing.Point(359, 371);
            this.bntVendedor.Name = "bntVendedor";
            this.bntVendedor.Size = new System.Drawing.Size(241, 50);
            this.bntVendedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntVendedor.TabIndex = 5;
            this.bntVendedor.TabStop = false;
            this.bntVendedor.Click += new System.EventHandler(this.bntVendedor_Click);
            // 
            // bntEstoque
            // 
            this.bntEstoque.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntEstoque.Image = ((System.Drawing.Image)(resources.GetObject("bntEstoque.Image")));
            this.bntEstoque.Location = new System.Drawing.Point(359, 315);
            this.bntEstoque.Name = "bntEstoque";
            this.bntEstoque.Size = new System.Drawing.Size(241, 50);
            this.bntEstoque.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntEstoque.TabIndex = 6;
            this.bntEstoque.TabStop = false;
            this.bntEstoque.Click += new System.EventHandler(this.bntEstoque_Click);
            // 
            // bntSair
            // 
            this.bntSair.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntSair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntSair.Image = ((System.Drawing.Image)(resources.GetObject("bntSair.Image")));
            this.bntSair.Location = new System.Drawing.Point(359, 427);
            this.bntSair.Name = "bntSair";
            this.bntSair.Size = new System.Drawing.Size(241, 50);
            this.bntSair.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntSair.TabIndex = 7;
            this.bntSair.TabStop = false;
            this.bntSair.Click += new System.EventHandler(this.bntSair_Click);
            // 
            // txtNome
            // 
            this.txtNome.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtNome.Enabled = false;
            this.txtNome.Location = new System.Drawing.Point(3, 3);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(122, 20);
            this.txtNome.TabIndex = 8;
            // 
            // frmMenu
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(599, 486);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.bntSair);
            this.Controls.Add(this.bntEstoque);
            this.Controls.Add(this.bntVendedor);
            this.Controls.Add(this.bntCompra);
            this.Controls.Add(this.bntFormapagamento);
            this.Controls.Add(this.bntFornecedor);
            this.Controls.Add(this.bntCliente);
            this.Controls.Add(this.bntAutomovel);
            this.Controls.Add(this.PctCabecalho);
            this.MaximizeBox = false;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PctCabecalho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAutomovel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntFornecedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntFormapagamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCompra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVendedor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntEstoque)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntSair)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PctCabecalho;
        private System.Windows.Forms.PictureBox bntAutomovel;
        private System.Windows.Forms.PictureBox bntCliente;
        private System.Windows.Forms.PictureBox bntFornecedor;
        private System.Windows.Forms.PictureBox bntFormapagamento;
        private System.Windows.Forms.PictureBox bntCompra;
        private System.Windows.Forms.PictureBox bntVendedor;
        private System.Windows.Forms.PictureBox bntEstoque;
        private System.Windows.Forms.PictureBox bntSair;
        private System.Windows.Forms.TextBox txtNome;
    }
}

