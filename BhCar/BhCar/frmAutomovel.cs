﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace BhCar
{
    public partial class frmAutomovel : Form
    {
        public frmAutomovel()
        {
            InitializeComponent();
        }
        conexaobd bd = new conexaobd();
        private void frmAutomovel_Load(object sender, EventArgs e)
        {
            string sel = "SELECT codigo, nome FROM fornecedor ORDER BY nome ASC";
            DataTable dt = bd.executarconsulta(sel);
            cmbFornecedor.DisplayMember = "nome";
            cmbFornecedor.ValueMember = "codigo";
            cmbFornecedor.DataSource = dt;
            // Verifica o usuario
            string query = "SELECT login.codigo, login.usuario, login.senha, nivel.descricao FROM login JOIN nivel ON nivel.codigo = login.nivel_codigo  WHERE login.usuario = '" + dadosdelogin.Usuario + "' AND nivel.descricao = @nivel";
            conexaobd conexao = new conexaobd();
            conexao.ConectarBD();

            MySqlCommand cmd = new MySqlCommand(query, conexao.con);

            cmd.Parameters.AddWithValue("@nivel", "Funcionário");

            MySqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                bntCadastrar.Visible = false;
                bntAlterar.Visible = false;
                bntExcluir.Visible = false;
                bntVoltar.Location = new Point(480, 588);
                bntPesquisar.Location = new Point(12, 588);

            }
                else if(dadosdelogin.Nivel == "Gerente")
                {
                    bntExcluir.Visible = false;
                    bntVoltar.Location = new Point(480, 588);
                }
            }

        private void bntVoltar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bntPesquisar_Click(object sender, EventArgs e)
        { 
                string sel = "SELECT automovel.codigo, automovel.chassi, automovel.preco, automovel.cor, automovel.cambio, automovel.versao, automovel.portas, automovel.motor, automovel.quilometragem, automovel.transmissao, automovel.combustivel, automovel.acessorios, automovel.ano, automovel.modelo, fornecedor.nome FROM automovel JOIN fornecedor ON fornecedor.codigo = fornecedor_codigo ORDER BY chassi DESC";
                DataTable dt = bd.executarconsulta(sel);
                dtgautomovel.DataSource = dt;
        }

        private void bntCadastrar_Click(object sender, EventArgs e)
        {
            if (txtmodelo.Text == "")
            {
                MessageBox.Show("Preencha o Campo Modelo!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmodelo.Focus();
            }
            else if ((txtano.Text.Length != 4) || (txtano.Text == ""))
            {
                MessageBox.Show("O Campo Ano é Inválido!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtano.Focus();
            }
            else if (txtcor.Text == "")
            {
                MessageBox.Show("Preencha o Campo Cor!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtcor.Focus();
            }
            else if (txtchassi.Text.Length != 14)
            {
                MessageBox.Show("O Chassi é Inválido!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtchassi.Focus();
            }
            else if (txttransmissao.Text == "")
            {
                MessageBox.Show("Preencha o Campo Transmissão!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttransmissao.Focus();
            }
            else if (txtmotor.Text == "")
            {
                MessageBox.Show("Preencha o Campo Motor!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmotor.Focus();
            }
            else if (txtversao.Text == "")
            {
                MessageBox.Show("Preencha o Campo Versão!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtversao.Focus();
            }
            else if (txtportas.Text == "")
            {
                MessageBox.Show("Insira um número de portas válido!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtportas.Focus();
            }
            else if (txtcambio.Text == "")
            {
                MessageBox.Show("Preencha o Campo Câmbio!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtcambio.Focus();
            }
            else if (txtquilometragem.Text == "")
            {
                MessageBox.Show("Preencha o Campo Quilometragem!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtquilometragem.Focus();
            }
            else if (txtcombustivel.Text == "")
            {
                MessageBox.Show("Preencha o Campo Combustível!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtcombustivel.Focus();
            }
            else if (txtpreco.Text == "")
            {
                MessageBox.Show("Preencha o Campo Preço \n ou retire o ponto(.)!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtpreco.Focus();
            }
            else
            {
                txtcodigo.Clear();
                string inserir;
                string acess = "";
                if (ckbAdesivo.Checked) acess += "Adesivo. ";
                if (ckbArCondicionado.Checked) acess += "Ar Condicionado. ";
                if (ckbBanco.Checked) acess += "Banco de Couro. ";
                if (ckbFarolneblina.Checked) acess += "Farol de Neblina. ";
                if (ckbGps.Checked) acess += "GPS. ";
                if (ckbMultimidia.Checked) acess += "Multimídia. ";
                if (ckbOrganizador.Checked) acess += "Organizador. ";
                if (ckbRadio.Checked) acess += "Rádio. ";
                if (ckbRoda.Checked) acess += "Roda de Liga leve. ";
                if (ckbSensor.Checked) acess += "Sensor de Estacionameto. ";
                if (ckbVidroele.Checked) acess += "Vidros e Travas Elétricas. ";
                if (ckbOutros.Checked) acess += txtOutros.Text;

                inserir = "insert into automovel (codigo, chassi, preco, cor, cambio, versao, portas, motor, quilometragem, transmissao, combustivel, acessorios, ano, modelo, fornecedor_codigo) ";
                inserir += " values (null, '" + txtchassi.Text + "'," + long.Parse(txtpreco.Text) + ",'" + txtcor.Text + "','" + txtcambio.Text + "','" + txtversao.Text + "', " + int.Parse(txtportas.Text) + ", '" + txtmotor.Text + "', " + long.Parse(txtquilometragem.Text) + ", '" + txttransmissao.Text + "', '" + txtcombustivel.Text + "', '" + acess + "'," + long.Parse(txtano.Text) + ",'" + txtmodelo.Text + "', " + cmbFornecedor.SelectedValue + ")";

                bd.executarcomandos(inserir);
                MessageBox.Show("Cadastro realizado com sucesso!");
                txtchassi.Clear();
                txtcor.Clear();
                txtcambio.Clear();
                txtversao.Clear();
                txtportas.Clear();
                txtmotor.Clear();
                txtquilometragem.Clear();
                txttransmissao.Clear();
                txtcombustivel.Clear();
                txtano.Clear();
                txtmodelo.Clear();
                txtpreco.Clear();
                txtOutros.Clear();
                txtchassi.Focus();
            }
          
        }

        private void dtgautomovel_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtcodigo.Text = dtgautomovel.Rows[e.RowIndex].Cells["codigo"].Value.ToString();
            txtchassi.Text = dtgautomovel.Rows[e.RowIndex].Cells["chassi"].Value.ToString();
            txtpreco.Text = dtgautomovel.Rows[e.RowIndex].Cells["preco"].Value.ToString();
            txtcor.Text = dtgautomovel.Rows[e.RowIndex].Cells["cor"].Value.ToString();
            txtcambio.Text = dtgautomovel.Rows[e.RowIndex].Cells["cambio"].Value.ToString();
            txtversao.Text = dtgautomovel.Rows[e.RowIndex].Cells["versao"].Value.ToString();
            txtportas.Text = dtgautomovel.Rows[e.RowIndex].Cells["portas"].Value.ToString();
            txtmotor.Text = dtgautomovel.Rows[e.RowIndex].Cells["motor"].Value.ToString();
            txtquilometragem.Text = dtgautomovel.Rows[e.RowIndex].Cells["quilometragem"].Value.ToString();
            txttransmissao.Text = dtgautomovel.Rows[e.RowIndex].Cells["transmissao"].Value.ToString();
            txtcombustivel.Text = dtgautomovel.Rows[e.RowIndex].Cells["combustivel"].Value.ToString();
            txtano.Text = dtgautomovel.Rows[e.RowIndex].Cells["ano"].Value.ToString();
            txtmodelo.Text = dtgautomovel.Rows[e.RowIndex].Cells["modelo"].Value.ToString();
            cmbFornecedor.Text = dtgautomovel.Rows[e.RowIndex].Cells["nome"].Value.ToString();
        }

        private void bntAlterar_Click(object sender, EventArgs e)
        {
            if (txtmodelo.Text == "")
            {
                MessageBox.Show("Preencha o Campo Modelo!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmodelo.Focus();
            }
            else if (txtano.Text.Length != 4)
            {
                MessageBox.Show("O Campo Ano é Inválido!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtano.Focus();
            }
            else if (txtcor.Text == "")
            {
                MessageBox.Show("Preencha o Campo Cor!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtcor.Focus();
            }
            else if (txtchassi.Text.Length != 14)
            {
                MessageBox.Show("O Chassi Ano é Inválido!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtchassi.Focus();
            }
            else if (txttransmissao.Text == "")
            {
                MessageBox.Show("Preencha o Campo Transmissão!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txttransmissao.Focus();
            }
            else if (txtmotor.Text == "")
            {
                MessageBox.Show("Preencha o Campo Motor!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtmotor.Focus();
            }
            else if (txtversao.Text == "")
            {
                MessageBox.Show("Preencha o Campo Versão!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtversao.Focus();
            }
            else if (txtportas.Text == "")
            {
                MessageBox.Show("Preencha o Campo Portas!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtportas.Focus();
            }
            else if (txtcambio.Text == "")
            {
                MessageBox.Show("Preencha o Campo Câmbio!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtcambio.Focus();
            }
            else if (txtquilometragem.Text == "")
            {
                MessageBox.Show("Preencha o Campo Quilometragem!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtquilometragem.Focus();
            }
            else if (txtcombustivel.Text == "")
            {
                MessageBox.Show("Preencha o Campo Combustível!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtcombustivel.Focus();
            }
            else if (txtpreco.Text == "")
            {
                MessageBox.Show("Preencha o Campo Preço \n ou retire o ponto(.)!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtpreco.Focus();
            }
            else
            {
                string alterar;
                string acess = "";
                if (ckbAdesivo.Checked) acess += "Adesivo. ";
                if (ckbArCondicionado.Checked) acess += "Ar Condicionado. ";
                if (ckbBanco.Checked) acess += "Banco de Couro. ";
                if (ckbFarolneblina.Checked) acess += "Farol de Neblina. ";
                if (ckbGps.Checked) acess += "GPS. ";
                if (ckbMultimidia.Checked) acess += "Multimídia. ";
                if (ckbOrganizador.Checked) acess += "Organizador. ";
                if (ckbRadio.Checked) acess += "Rádio. ";
                if (ckbRoda.Checked) acess += "Roda de Liga leve. ";
                if (ckbSensor.Checked) acess += "Sensor de Estacionameto. ";
                if (ckbVidroele.Checked) acess += "Vidros e Travas Elétricas. ";
                if (ckbOutros.Checked) { txtOutros.Enabled = true; acess += txtOutros.Text; }

                alterar = "update automovel set chassi ='" + txtchassi.Text + "', preco =" + long.Parse(txtpreco.Text) + ", cor ='" + txtcor.Text + "', cambio ='" + txtcambio.Text + "', versao ='" + txtversao.Text + "', portas =" + int.Parse(txtportas.Text) + ", motor ='" + txtmotor.Text + "', quilometragem =" + long.Parse(txtquilometragem.Text) + ", transmissao ='" + txttransmissao.Text + "', combustivel ='" + txtcombustivel.Text + "', acessorios ='" + acess + "', ano =" + long.Parse(txtano.Text) + ", modelo ='" + txtmodelo.Text + "', fornecedor_codigo =" + cmbFornecedor.SelectedValue + " where codigo = " + long.Parse(txtcodigo.Text);

                bd.executarcomandos(alterar);
                MessageBox.Show("Cadastro Alterado com sucesso!");
                txtchassi.Clear();
                txtcor.Clear();
                txtcambio.Clear();
                txtversao.Clear();
                txtportas.Clear();
                txtmotor.Clear();
                txtquilometragem.Clear();
                txttransmissao.Clear();
                txtcombustivel.Clear();
                txtano.Clear();
                txtmodelo.Clear();
                txtpreco.Clear();
                txtOutros.Clear();
                txtchassi.Focus();
            }
        }

        private void bntExcluir_Click(object sender, EventArgs e)
        {
                if (txtcodigo.Text == "")
                {
                    MessageBox.Show("Selecione o Campo", "Confirmação",
                 MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    string sel = "SELECT *FROM automovel order by chassi";
                    DataTable dt = bd.executarconsulta(sel);
                    dtgautomovel.DataSource = dt;

                }
                else if (MessageBox.Show("Tem certeza que deseja Excluir o cadastro?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string excluir;
                    excluir = "delete from automovel where codigo = '" + txtcodigo.Text + "'";
                    bd.executarcomandos(excluir);
                    MessageBox.Show("Cadastro Excluido com sucesso!");
                    txtchassi.Clear();
                    txtcor.Clear();
                    txtcambio.Clear();
                    txtversao.Clear();
                    txtportas.Clear();
                    txtmotor.Clear();
                    txtquilometragem.Clear();
                    txttransmissao.Clear();
                    txtcombustivel.Clear();
                    txtano.Clear();
                    txtmodelo.Clear();
                    txtpreco.Clear();
                    txtchassi.Focus();
                }
        }
    }
}
