﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace BhCar
{
    public partial class frmEstoque : Form
    {
        public frmEstoque()
        {
            InitializeComponent();
        }
        conexaobd bd = new conexaobd();

        private void bntVoltar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dtgEstoque_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtCodigo.Text = dtgEstoque.Rows[e.RowIndex].Cells["codigo"].Value.ToString();
            txtQuantidade.Text = dtgEstoque.Rows[e.RowIndex].Cells["quantidade"].Value.ToString();
            cmbAutomovel.Text = dtgEstoque.Rows[e.RowIndex].Cells["modelo"].Value.ToString();
        }

        private void bntCadastrar_Click(object sender, EventArgs e)
        {
            if (txtQuantidade.Text == "")
            {
                MessageBox.Show("Preencha o Campo Quantidade", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtQuantidade.Focus();
            }
            else if (txtData.Text.Length != 10)
            {
                MessageBox.Show("O Campo Data é Inválido", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtData.Focus();
            }
            else
            {
                txtCodigo.Clear();
                string data;
                data = txtData.Value.ToString("yyyy-MM-dd"); 
                string inserir;
                inserir = " INSERT INTO estoque (codigo, quantidade, data_atualizacao, automovel_codigo) VALUES (NULL, " + long.Parse(txtQuantidade.Text) + ",'" + data + "'," + cmbAutomovel.SelectedValue + ")";
                bd.executarcomandos(inserir);
                MessageBox.Show("Cadastro Realizado com sucesso!");
                txtQuantidade.Clear();
                txtQuantidade.Focus();
            }

        }

        private void frmEstoque_Load(object sender, EventArgs e)
        {
            string sel = "SELECT codigo, modelo FROM automovel ORDER BY modelo ASC";
            DataTable dt = bd.executarconsulta(sel);
            cmbAutomovel.DisplayMember = "modelo";
            cmbAutomovel.ValueMember = "codigo";
            cmbAutomovel.DataSource = dt;

            string query = "SELECT login.codigo, login.usuario, login.senha, nivel.descricao FROM login JOIN nivel ON nivel.codigo = login.nivel_codigo  WHERE login.usuario = '" + dadosdelogin.Usuario + "' AND nivel.descricao = @nivel";
            conexaobd conexao = new conexaobd();
            conexao.ConectarBD();

            MySqlCommand cmd = new MySqlCommand(query, conexao.con);

            cmd.Parameters.AddWithValue("@nivel", "Gerente");

            MySqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                bntExcluir.Visible = false;
                bntVoltar.Location = new Point(475, 388);
            }
        }

        private void bntAlterar_Click(object sender, EventArgs e)
        {
            if (txtQuantidade.Text == "")
            {
                MessageBox.Show("Preencha o Campo Quantidade", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtQuantidade.Focus();
            }
            else if (txtData.Text.Length != 10)
            {
                MessageBox.Show("O Campo Data é Inválido", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtData.Focus();
            }
            else if (txtCodigo.Text == "")
            {
                MessageBox.Show("Selecione o Campo", "Erro",
             MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {
                string data;
                data = txtData.Value.ToString("yyyy-MM-dd");
                string alterar;
                alterar = "UPDATE estoque SET  quantidade = " + long.Parse(txtQuantidade.Text) + ", data_atualizacao = '" + data + "', automovel_codigo = " + cmbAutomovel.SelectedValue + " where codigo = " + long.Parse(txtCodigo.Text);
                bd.executarcomandos(alterar);
                MessageBox.Show("Cadastro Alterado com sucesso!");
                txtCodigo.Clear();
                txtQuantidade.Clear();
                txtQuantidade.Focus();
            }
        }

        private void bntExcluir_Click(object sender, EventArgs e)
        {
                if (txtCodigo.Text == "")
                {
                    MessageBox.Show("Selecione o Campo", "Erro",
                 MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    string sel = "SELECT * FROM estoque";
                    DataTable dt = bd.executarconsulta(sel);
                    dtgEstoque.DataSource = dt;
                }
                else if (MessageBox.Show("Tem certeza que deseja Excluir o cadastro?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string excluir;
                    excluir = "delete from estoque where codigo = " + long.Parse(txtCodigo.Text) + "";
                    bd.executarcomandos(excluir);
                    MessageBox.Show("Cadastro Excluido com sucesso!");
                    txtCodigo.Clear();
                    txtQuantidade.Clear();
                    txtQuantidade.Focus();
                }
        }

        private void bntPesquisar_Click(object sender, EventArgs e)
        {
            string sel = "SELECT estoque.codigo, estoque.quantidade, estoque.data_atualizacao, automovel.modelo FROM estoque JOIN automovel ORDER BY estoque.data_atualizacao DESC";
            DataTable dt = bd.executarconsulta(sel);
            dtgEstoque.DataSource = dt;
        }
    }
}
