﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;


namespace BhCar
{
    class conexaobd
    {
       //String de Conexão
        public MySqlConnection con = new MySqlConnection();
        public void ConectarBD()
        {
            try
            {
                con = new MySqlConnection("Persist Security Info = false; server = localhost; database = bhcar; user = root; pwd=;");
                con.Open();
            }

            catch (Exception)
            {
                throw;
            }
        }

        //Comandos de Insert, Delete, Update
        public void executarcomandos(string sql)
        {
            try
            {
                ConectarBD();
                MySqlCommand cmd = new MySqlCommand(sql, con);
                cmd.ExecuteNonQuery();
            }

            catch (Exception)
            {
                throw;
            }

            finally
            {
                con.Close();
            }
        }

        //Executar Consulta

        public DataTable executarconsulta(string sql)
        {


            try
            {
                ConectarBD();
                MySqlDataAdapter da = new MySqlDataAdapter(sql, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
            
                return dt;
            }

            catch (Exception)
            {
                throw;
            }

            finally
            {
                con.Close();
            }


        }

        public static bool Verificarcpf(string cpf){

            int[] v1 = new int[9] {10, 9, 8, 7, 6, 5, 4, 3, 2};
            int[] v2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            string x, y;
            int soma, resultado;

            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            x = cpf.Substring(0, 9);
            soma = 0;

            for (int l = 0; l < 9; l++)
                soma += int.Parse(x[l].ToString()) * v1[l];
            resultado = soma % 11;
            if (resultado < 2)
                resultado = 0;
            else
                resultado = 11 - resultado;
            y = resultado.ToString();
            x = x + y;

            soma = 0;
            for (int l = 0; l < 10; l++)
                soma += int.Parse(x[l].ToString()) * v2[l];
            resultado = soma % 11;
            if (resultado < 2)
                resultado = 0;
            else
                resultado = 11 - resultado;
            y = y + resultado.ToString();

            return cpf.EndsWith(y);

        }
    }
}

