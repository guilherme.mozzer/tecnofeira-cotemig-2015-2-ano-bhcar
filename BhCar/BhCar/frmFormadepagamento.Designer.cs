﻿namespace BhCar
{
    partial class frmFormadepagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFormadepagamento));
            this.pictureBoxCabecalho = new System.Windows.Forms.PictureBox();
            this.txtdescricao = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtcodigo = new System.Windows.Forms.TextBox();
            this.dtgPagamento = new System.Windows.Forms.DataGridView();
            this.bntVoltar = new System.Windows.Forms.PictureBox();
            this.bntPesquisar = new System.Windows.Forms.PictureBox();
            this.bntAlterar = new System.Windows.Forms.PictureBox();
            this.bntExcluir = new System.Windows.Forms.PictureBox();
            this.bntCadastrar = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.lblObrigatorio = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCabecalho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPagamento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVoltar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntPesquisar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAlterar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntExcluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCadastrar)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxCabecalho
            // 
            this.pictureBoxCabecalho.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCabecalho.Image")));
            this.pictureBoxCabecalho.Location = new System.Drawing.Point(0, 1);
            this.pictureBoxCabecalho.Name = "pictureBoxCabecalho";
            this.pictureBoxCabecalho.Size = new System.Drawing.Size(642, 73);
            this.pictureBoxCabecalho.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCabecalho.TabIndex = 3;
            this.pictureBoxCabecalho.TabStop = false;
            // 
            // txtdescricao
            // 
            this.txtdescricao.Location = new System.Drawing.Point(405, 80);
            this.txtdescricao.MaxLength = 45;
            this.txtdescricao.Multiline = true;
            this.txtdescricao.Name = "txtdescricao";
            this.txtdescricao.Size = new System.Drawing.Size(225, 20);
            this.txtdescricao.TabIndex = 201;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(293, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 13);
            this.label5.TabIndex = 204;
            this.label5.Text = "* Tipo do Pagamento :";
            // 
            // txtcodigo
            // 
            this.txtcodigo.Enabled = false;
            this.txtcodigo.Location = new System.Drawing.Point(63, 80);
            this.txtcodigo.MaxLength = 45;
            this.txtcodigo.Name = "txtcodigo";
            this.txtcodigo.Size = new System.Drawing.Size(191, 20);
            this.txtcodigo.TabIndex = 203;
            // 
            // dtgPagamento
            // 
            this.dtgPagamento.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dtgPagamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPagamento.Location = new System.Drawing.Point(15, 127);
            this.dtgPagamento.Name = "dtgPagamento";
            this.dtgPagamento.Size = new System.Drawing.Size(618, 207);
            this.dtgPagamento.TabIndex = 202;
            this.dtgPagamento.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgPagamento_CellContentClick);
            // 
            // bntVoltar
            // 
            this.bntVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntVoltar.Image = ((System.Drawing.Image)(resources.GetObject("bntVoltar.Image")));
            this.bntVoltar.Location = new System.Drawing.Point(483, 386);
            this.bntVoltar.Name = "bntVoltar";
            this.bntVoltar.Size = new System.Drawing.Size(150, 40);
            this.bntVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntVoltar.TabIndex = 209;
            this.bntVoltar.TabStop = false;
            this.bntVoltar.Click += new System.EventHandler(this.bntVoltar_Click);
            // 
            // bntPesquisar
            // 
            this.bntPesquisar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("bntPesquisar.Image")));
            this.bntPesquisar.Location = new System.Drawing.Point(171, 340);
            this.bntPesquisar.Name = "bntPesquisar";
            this.bntPesquisar.Size = new System.Drawing.Size(150, 40);
            this.bntPesquisar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntPesquisar.TabIndex = 208;
            this.bntPesquisar.TabStop = false;
            this.bntPesquisar.Click += new System.EventHandler(this.bntPesquisar_Click);
            // 
            // bntAlterar
            // 
            this.bntAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntAlterar.Image = ((System.Drawing.Image)(resources.GetObject("bntAlterar.Image")));
            this.bntAlterar.Location = new System.Drawing.Point(327, 340);
            this.bntAlterar.Name = "bntAlterar";
            this.bntAlterar.Size = new System.Drawing.Size(150, 40);
            this.bntAlterar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntAlterar.TabIndex = 207;
            this.bntAlterar.TabStop = false;
            this.bntAlterar.Click += new System.EventHandler(this.bntAlterar_Click);
            // 
            // bntExcluir
            // 
            this.bntExcluir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntExcluir.Image = ((System.Drawing.Image)(resources.GetObject("bntExcluir.Image")));
            this.bntExcluir.Location = new System.Drawing.Point(483, 340);
            this.bntExcluir.Name = "bntExcluir";
            this.bntExcluir.Size = new System.Drawing.Size(150, 40);
            this.bntExcluir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntExcluir.TabIndex = 206;
            this.bntExcluir.TabStop = false;
            this.bntExcluir.Click += new System.EventHandler(this.bntExcluir_Click);
            // 
            // bntCadastrar
            // 
            this.bntCadastrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntCadastrar.Image = ((System.Drawing.Image)(resources.GetObject("bntCadastrar.Image")));
            this.bntCadastrar.Location = new System.Drawing.Point(15, 340);
            this.bntCadastrar.Name = "bntCadastrar";
            this.bntCadastrar.Size = new System.Drawing.Size(150, 40);
            this.bntCadastrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntCadastrar.TabIndex = 205;
            this.bntCadastrar.TabStop = false;
            this.bntCadastrar.Click += new System.EventHandler(this.bntCadastrar_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 83);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 13);
            this.label12.TabIndex = 210;
            this.label12.Text = "codigo :";
            // 
            // lblObrigatorio
            // 
            this.lblObrigatorio.AutoSize = true;
            this.lblObrigatorio.Location = new System.Drawing.Point(522, 111);
            this.lblObrigatorio.Name = "lblObrigatorio";
            this.lblObrigatorio.Size = new System.Drawing.Size(111, 13);
            this.lblObrigatorio.TabIndex = 211;
            this.lblObrigatorio.Text = "* Campos Obrigatórios";
            // 
            // frmFormadepagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 438);
            this.Controls.Add(this.lblObrigatorio);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.bntVoltar);
            this.Controls.Add(this.bntPesquisar);
            this.Controls.Add(this.bntAlterar);
            this.Controls.Add(this.bntExcluir);
            this.Controls.Add(this.bntCadastrar);
            this.Controls.Add(this.txtdescricao);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtcodigo);
            this.Controls.Add(this.dtgPagamento);
            this.Controls.Add(this.pictureBoxCabecalho);
            this.MaximizeBox = false;
            this.Name = "frmFormadepagamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Forma de Pagamento";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCabecalho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPagamento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVoltar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntPesquisar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAlterar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntExcluir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCadastrar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxCabecalho;
        private System.Windows.Forms.TextBox txtdescricao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtcodigo;
        private System.Windows.Forms.DataGridView dtgPagamento;
        private System.Windows.Forms.PictureBox bntVoltar;
        private System.Windows.Forms.PictureBox bntPesquisar;
        private System.Windows.Forms.PictureBox bntAlterar;
        private System.Windows.Forms.PictureBox bntExcluir;
        private System.Windows.Forms.PictureBox bntCadastrar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblObrigatorio;
    }
}