﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace BhCar
{
    public partial class frmCompra : Form
    {
        public frmCompra()
        {
            InitializeComponent();
        }
        conexaobd bd = new conexaobd();
        private void bntVoltar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmCompra_Load(object sender, EventArgs e)
        {
            if (cmbcliente.Text == "")
            {
                string sel = "SELECT codigo, nome FROM cliente ORDER BY nome ASC";
                DataTable dt = bd.executarconsulta(sel);
                cmbcliente.DisplayMember = "nome";
                cmbcliente.ValueMember = "codigo";
                cmbcliente.DataSource = dt;
            }
            if (cmbautomovel.Text == "")
            {
                string sele = "SELECT codigo, modelo FROM automovel ORDER BY modelo ASC";
                DataTable dt = bd.executarconsulta(sele);
                cmbautomovel.DisplayMember = "modelo";
                cmbautomovel.ValueMember = "codigo";
                cmbautomovel.DataSource = dt;
            }
            if (cmbvendedor.Text == "")
            {
                string selec = "SELECT codigo, nome FROM vendedor ORDER BY nome ASC";
                DataTable dt = bd.executarconsulta(selec);
                cmbvendedor.DisplayMember = "nome";
                cmbvendedor.ValueMember = "codigo";
                cmbvendedor.DataSource = dt;
            }
            if (cmbPagamento.Text == "")
            {
                string select = "SELECT codigo, descricao FROM forma_de_pagamento ORDER BY descricao ASC";
                DataTable dt = bd.executarconsulta(select);
                cmbPagamento.DisplayMember = "descricao";
                cmbPagamento.ValueMember = "codigo";
                cmbPagamento.DataSource = dt;
            }

            string query = "SELECT login.codigo, login.usuario, login.senha, nivel.descricao FROM login JOIN nivel ON nivel.codigo = login.nivel_codigo  WHERE login.usuario = '" + dadosdelogin.Usuario + "' AND nivel.descricao = @nivel";
            conexaobd conexao = new conexaobd();
            conexao.ConectarBD();

            MySqlCommand cmd = new MySqlCommand(query, conexao.con);

            cmd.Parameters.AddWithValue("@nivel", "Funcionário");

            MySqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                bntAlterar.Visible = false;
                bntExcluir.Visible = false;
                bntVoltar.Location = new Point(490, 451);

            }
            else if (dadosdelogin.Nivel == "Gerente")
            {
                bntExcluir.Visible = false;
                bntVoltar.Location = new Point(490, 451);
            }
        }

        private void dtgCompra_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtCodigo.Text = dtgCompra.Rows[e.RowIndex].Cells["codigo"].Value.ToString();
            txtPreco.Text = dtgCompra.Rows[e.RowIndex].Cells["preco"].Value.ToString();
            txtQuantParcelas.Text = dtgCompra.Rows[e.RowIndex].Cells["n_parcela"].Value.ToString();
            txtData.Text = dtgCompra.Rows[e.RowIndex].Cells["data_compra"].Value.ToString();
            txtDesconto.Text = dtgCompra.Rows[e.RowIndex].Cells["desconto"].Value.ToString();
            cmbcliente.Text = dtgCompra.Rows[e.RowIndex].Cells["nome"].Value.ToString();
            cmbautomovel.Text = dtgCompra.Rows[e.RowIndex].Cells["modelo"].Value.ToString();
            cmbPagamento.Text = dtgCompra.Rows[e.RowIndex].Cells["descricao"].Value.ToString();
            cmbvendedor.Text = dtgCompra.Rows[e.RowIndex].Cells["nome1"].Value.ToString();
        }

        private void bntPesquisar_Click(object sender, EventArgs e)
        {
            string sel = "SELECT compra.codigo, compra.preco, compra.n_parcela, compra.data_compra, compra.desconto, cliente.nome, automovel.modelo, forma_de_pagamento.descricao, vendedor.nome FROM compra JOIN cliente JOIN automovel JOIN forma_de_pagamento JOIN vendedor GROUP BY compra.codigo ORDER BY compra.data_compra DESC";
            DataTable dt = bd.executarconsulta(sel);
            dtgCompra.DataSource = dt;
        }

        private void bntExcluir_Click(object sender, EventArgs e)
        {
                if (txtCodigo.Text == "")
                {
                    MessageBox.Show("Selecione o Campo", "Erro",
                 MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    string sel = "SELECT * FROM compra ORDER BY data_compra ASC";
                    DataTable dt = bd.executarconsulta(sel);
                    dtgCompra.DataSource = dt;
                }
                else if (MessageBox.Show("Tem certeza que deseja Excluir o cadastro?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    string excluir;
                    excluir = "delete from compra where codigo = " + long.Parse(txtCodigo.Text) + "";
                    bd.executarcomandos(excluir);
                    MessageBox.Show("Cadastro Excluido com sucesso!");
                    txtCodigo.Clear();
                    txtPreco.Clear();
                    txtQuantParcelas.Clear();
                    txtDesconto.Clear();
                    txtQuantParcelas.Focus();

                }
        }

        private void bntCadastrar_Click(object sender, EventArgs e)
        {
            if (txtQuantParcelas.Text == "")
            {
                MessageBox.Show("Preencha o Campo Quantidade de Parcelas", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtQuantParcelas.Focus();
            }
            else if (txtPreco.Text == "")
            {
                MessageBox.Show("Preencha o Campo Preço", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtPreco.Focus();
            }
            else if (txtDesconto.Text == "")
            {
                MessageBox.Show("Preencha o Campo Desconto", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtDesconto.Focus();
            }
            else if (txtData.Text.Length != 10)
            {
                MessageBox.Show("O Campo Data é Inválido", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtData.Focus();
            }
            else if (cmbcliente.Text == "")
            {
                MessageBox.Show("Selecione o Cliente", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbcliente.Focus();
            }
            else if (cmbautomovel.Text == "")
            {
                MessageBox.Show("Selecione o Automóvel", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbautomovel.Focus();
            }
            else if (cmbvendedor.Text == "")
            {
                MessageBox.Show("Selecione o Vendedor", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbvendedor.Focus();
            }
            else if (cmbPagamento.Text == "")
            {
                MessageBox.Show("Selecione a Forma de Pagamento", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                cmbPagamento.Focus();
            }
            else
            {
                txtCodigo.Clear();
                string data;
                data = txtData.Value.ToString("yyyy-MM-dd");
                string inserir;
                inserir = " INSERT INTO compra (codigo, preco, n_parcela, data_compra, desconto, cliente_codigo, automovel_codigo, forma_de_pagamento_codigo, vendedor_codigo) VALUES(NULL, "+long.Parse(txtPreco.Text)+", "+long.Parse(txtQuantParcelas.Text)+", '"+data+"', "+long.Parse(txtDesconto.Text)+", "+cmbcliente.SelectedValue+","+cmbautomovel.SelectedValue+","+cmbPagamento.SelectedValue + ","+cmbvendedor.SelectedValue + ")";
                bd.executarcomandos(inserir);
                MessageBox.Show("Cadastro Realizado com sucesso!");
                txtPreco.Clear();
                txtQuantParcelas.Clear();
                txtDesconto.Clear();
                txtQuantParcelas.Focus();
            }
        }

        private void bntAlterar_Click(object sender, EventArgs e)
        {
                if (txtQuantParcelas.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Quantidade de Parcelas", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtQuantParcelas.Focus();
                }
                else if (txtPreco.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Preço", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPreco.Focus();
                }
                else if (txtDesconto.Text == "")
                {
                    MessageBox.Show("Preencha o Campo Desconto", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtDesconto.Focus();
                }
                else if (txtData.Text.Length != 10)
                {
                    MessageBox.Show("O Campo Data é Inválido", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtData.Focus();
                }
                else if (cmbcliente.Text == "")
                {
                    MessageBox.Show("Selecione o Cliente", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cmbcliente.Focus();
                }
                else if (cmbautomovel.Text == "")
                {
                    MessageBox.Show("Selecione o Automóvel", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cmbautomovel.Focus();
                }
                else if (cmbvendedor.Text == "")
                {
                    MessageBox.Show("Selecione o Vendedor", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cmbvendedor.Focus();
                }
                else if (cmbPagamento.Text == "")
                {
                    MessageBox.Show("Selecione a Forma de Pagamento", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cmbPagamento.Focus();
                }
                else
                {
                    string data;
                    data = txtData.Value.ToString("yyyy-MM-dd");
                    string alterar;
                    alterar = "UPDATE compra SET preco = " + long.Parse(txtPreco.Text) + ", n_parcela = " + long.Parse(txtQuantParcelas.Text) + ",data_compra = '" + data + "', desconto = " + long.Parse(txtDesconto.Text) + ", cliente_codigo = " + cmbcliente.SelectedValue + ", automovel_codigo = " + cmbautomovel.SelectedValue + ", forma_de_pagamento_codigo = " + cmbPagamento.SelectedValue + ", vendedor_codigo = " + cmbvendedor.SelectedValue + " where codigo = " + long.Parse(txtCodigo.Text);
                    bd.executarcomandos(alterar);
                    MessageBox.Show("Cadastro Alterado com sucesso!");
                    txtCodigo.Clear();
                    txtPreco.Clear();
                    txtQuantParcelas.Clear();
                    txtDesconto.Clear();
                    txtQuantParcelas.Focus();
                }
        }

        private void cmbautomovel_SelectedIndexChanged(object sender, EventArgs e)
        {
           //preencher preço a partir do combobox
        }
    }
}
