só copiar e colar;

DROP DATABASE bhcar;
CREATE database bhcar;
USE bhcar;

CREATE TABLE nivel (
codigo int unsigned auto_increment not null,
descricao VARCHAR(30) NOT NULL,
PRIMARY KEY(codigo)
);


CREATE TABLE login (
codigo INT UNSIGNED AUTO_INCREMENT NOT NULL,
usuario VARCHAR(100) NOT NULL,
senha VARCHAR(20) NOT NULL,
nivel_codigo int unsigned not null,
PRIMARY KEY(codigo),
foreign key (nivel_codigo) references nivel(codigo) on update cascade on delete cascade
);

CREATE TABLE vendedor(
codigo INT UNSIGNED AUTO_INCREMENT NOT NULL,
nome VARCHAR(100) NOT NULL,
sexo CHAR(1) NOT NULL,
email VARCHAR(80) NOT NULL,
telefone VARCHAR(14) NOT NULL,
PRIMARY KEY(codigo)
);


CREATE TABLE fornecedor(
codigo int unsigned auto_increment not null,
nome VARCHAR(100) NOT NULL,
marca VARCHAR(45) NOT NULL,
email VARCHAR(80) NOT NULL,
tel VARCHAR(14) NOT NULL,
PRIMARY KEY(codigo)
);

CREATE TABLE cliente( 
codigo int unsigned auto_increment not null,
nome varchar(100) not null,
cpf varchar(14) not null,
sexo CHAR(1) NOT NULL,
rg varchar(13) not null,
email varchar(80) not null,
logradouro varchar(70) not null,
numero mediumint not null,
complemento varchar(20) null,
bairro varchar(50)  not null,
cidade varchar(60)  not null,
uf char(2)  not null,
cep varchar(9) null,
comprovante_de_renda varchar(45) not null,
Comprovante_de_endereco varchar(100) not null,
telefone VARCHAR(14) NOT NULL,
primary key(codigo)
);

create table forma_de_pagamento(
codigo int unsigned auto_increment not null,
descricao VARCHAR(30) NOT NULL,
PRIMARY KEY(codigo)
);

create table automovel(
codigo int unsigned auto_increment not null,
chassi varchar(17) not null,
preco DOUBLE NOT NULL,
cor varchar(80) not null,
cambio varchar(30) not null,
versao varchar(80) not null,
portas int not null,
motor varchar(100) not null,
quilometragem bigint not null,
transmissao varchar(50) not null,
combustivel varchar(50) not null,
acessorios varchar(100) not null,
ano year not null,
modelo varchar(100) not null,
fornecedor_codigo int unsigned not null,
primary key(codigo),
foreign key (fornecedor_codigo) references fornecedor(codigo) on update cascade on delete cascade
);

create table estoque(
codigo int unsigned auto_increment not null,
quantidade int not null,
data_atualizacao date null,
automovel_codigo int unsigned not null,
primary key (codigo),
foreign key (automovel_codigo) references automovel(codigo) on update cascade on delete cascade
);

create table compra(
codigo INT UNSIGNED AUTO_INCREMENT NOT NULL,
preco DOUBLE NOT NULL,
n_parcela INT NULL,
data_compra DATE NOT NULL,
desconto DOUBLE NOT NULL,
cliente_codigo int unsigned not null,
automovel_codigo int unsigned not null,
forma_de_pagamento_codigo int unsigned not null,
vendedor_codigo int unsigned not null,
primary key(codigo),
foreign key (cliente_codigo) references cliente(codigo) on update cascade on delete cascade,
foreign key (automovel_codigo) references automovel(codigo) on update cascade on delete cascade,
foreign key (vendedor_codigo) references vendedor(codigo) on update cascade on delete cascade,
foreign key (forma_de_pagamento_codigo) references forma_de_pagamento(codigo) on update cascade on delete cascade
);

insert into nivel (codigo, descricao)
VALUES(NULL,'Administrador');
insert into nivel (codigo, descricao)
VALUES(NULL,'Gerente');
insert into nivel (codigo, descricao)
VALUES(NULL,'Funcionário');

insert into login (codigo, usuario, senha, nivel_codigo)
VALUES(NULL,'Carlos', '1234567890', 1);
insert into login (codigo, usuario, senha, nivel_codigo)
VALUES(NULL,'Lucas', '1234567890', 2);
insert into login (codigo, usuario, senha, nivel_codigo)
VALUES(NULL,'Joao', '1234567890', 3);
insert into login (codigo, usuario, senha, nivel_codigo)
VALUES(NULL,'Gabriel', '1234567890', 3);
insert into login (codigo, usuario, senha, nivel_codigo)
VALUES(NULL,'Carla', '1234567890', 3);


insert into vendedor (codigo,nome,sexo,email,telefone)
Values (null,'Matheus Silva','m','sila@gmail.com','(31)998756541');
insert into vendedor (codigo,nome,sexo,email,telefone)
Values (null,'Ana Ferreira','f','ferreira_ana@hotmail.com','(31)998750014');
insert into vendedor (codigo,nome,sexo,email,telefone)
Values (null,'Thiago Nunes','m','thiagonunes@yahoo.com','(31)9988556541');
insert into vendedor (codigo,nome,sexo,email,telefone)
Values (null,'Clara Marques','f','claramarques@gmail.com','(31)975456541');
insert into vendedor (codigo,nome,sexo,email,telefone)
Values (null,'Samuel Souza','m','samuka@gmail.com','(31)988003241');

insert into fornecedor(codigo,nome,marca,email,tel)
values(null,'ExpressCar','Chevrolet','expresscar@gmail.com','(31)935658129');
insert into fornecedor(codigo,nome,marca,email,tel)
values(null,'Scuderia','Fiat','scuderia@gmail.com','(31)935658129');
insert into fornecedor(codigo,nome,marca,email,tel)
values(null,'Forlan','Ford','fordforlan@gmail.com','(31)935658129');
insert into fornecedor(codigo,nome,marca,email,tel)
values(null,'BanzaiHonda','Honda','banzaihonda@gmail.com','(31)935658129');
insert into fornecedor(codigo,nome,marca,email,tel)
values(null,'VolksCar','Volkswagen','@gmail.com','(31)935658129');

insert into cliente (codigo,nome,cpf,sexo,rg,email,logradouro,numero,complemento,bairro,cidade,uf,cep,comprovante_de_renda,comprovante_de_endereco,telefone)
values(null,'Claudio Pacheco',02156998720,'m','mg 167418889','pachequinho@gmail.com','Rua Silva Lopes',196,null,'Barreiro','Belo Horizonte','mg','30662072',3.200,'Conta de Luz','(31)975846231');
insert into cliente (codigo,nome,cpf,sexo,rg,email,logradouro,numero,complemento,bairro,cidade,uf,cep,comprovante_de_renda,comprovante_de_endereco,telefone)
values(null,'Fernanda Lima',02897998720,'f','mg 265988889','felima@gmail.com','Rua Dê',258,null,'Tirol','Belo Horizonte','mg','30601272',2.500,'Conta de Agua','(31)998746274');
insert into cliente (codigo,nome,cpf,sexo,rg,email,logradouro,numero,complemento,bairro,cidade,uf,cep,comprovante_de_renda,comprovante_de_endereco,telefone)
values(null,'Douglas Alexandre',02156997894,'m','mg 167445782','xande@gmail.com','Rua Calateia',389,null,'Centro','Betim','mg','40662123',4.000,'Conta de Luz','(31)975559798');
insert into cliente (codigo,nome,cpf,sexo,rg,email,logradouro,numero,complemento,bairro,cidade,uf,cep,comprovante_de_renda,comprovante_de_endereco,telefone)
values(null,'Alexandre Frotta',04556993690,'m','mg 19148889','frotinha@gmail.com','Rua Sekisabi',287,null,'Nova Suissa','Belo Horizonte','mg','34888072',10.000,'Conta de Internet','(31)979876214');
insert into cliente (codigo,nome,cpf,sexo,rg,email,logradouro,numero,complemento,bairro,cidade,uf,cep,comprovante_de_renda,comprovante_de_endereco,telefone)
values(null,'Augusto Curi',04789298720,'m','mg 182345678','curiaugusto@gmail.com','Rua Serpentina',100,null,'Oração','Nova Lima','mg','39874572',12.000,'Conta de Agua','(31)995844561');

insert into forma_de_pagamento(codigo,descricao)
values(null,'A vista');
insert into forma_de_pagamento(codigo,descricao)
values(null,'Financiameto');
insert into forma_de_pagamento(codigo,descricao)
values(null,'Cheque');
insert into forma_de_pagamento(codigo,descricao)
values(null,'Parcelado');
insert into forma_de_pagamento(codigo,descricao)
values(null,'Cartão');

insert into automovel(codigo,chassi,preco,cor,cambio,versao,portas,motor,quilometragem,transmissao,combustivel,acessorios,ano,modelo,fornecedor_codigo)
values (null,'9PWAAA377VT009881',22000,'Branco','Automatico','evo vivace',4,'1.0',43.590,'Automatico','Gasolina','Radio,GPS,AR Condicionado','2013','Fiat UNO',2);
insert into automovel(codigo,chassi,preco,cor,cambio,versao,portas,motor,quilometragem,transmissao,combustivel,acessorios,ano,modelo,fornecedor_codigo)
values (null,'9BWZZZ377VT004271',236000,'Preto','Automatico','SS',2,'2.0 turbo',0,'Automatico','Gasolina','GPS,Camera de Ré,Radio,AR Condicionado,Banco de couro','2016','Camaro SS',1);
insert into automovel(codigo,chassi,preco,cor,cambio,versao,portas,motor,quilometragem,transmissao,combustivel,acessorios,ano,modelo,fornecedor_codigo)
values (null,'9CABBB807VT000251',110000,'Vermelho','Automatico','Cornfort flex ',4,'2.0',0,'Automatico','Gasolina','GPS,Radio,AR Condicionado,Banco de couro','2015','Ford Fusion',3);
insert into automovel(codigo,chassi,preco,cor,cambio,versao,portas,motor,quilometragem,transmissao,combustivel,acessorios,ano,modelo,fornecedor_codigo)
values (null,'9GJRRR234VT009259',16900,'Amarelo','Manual','lx 16v',2,'1.7',100.000,'Manual','Gasolina','Radio','2002','Honda Civic',4);
insert into automovel(codigo,chassi,preco,cor,cambio,versao,portas,motor,quilometragem,transmissao,combustivel,acessorios,ano,modelo,fornecedor_codigo)
values (null,'9SXLLL377VT004760',86790,'Prata','Automatico','confortline flex 4p tiptronic',4,'2.0',0,'Automatico','Gasolina','GPS, Radio','2015','Volkswagen JETTA',5);

insert into estoque(codigo,quantidade,data_atualizacao,automovel_codigo)
values(null,2,"2015/07/23",3);
insert into estoque(codigo,quantidade,data_atualizacao,automovel_codigo)
values(null,1,"2015/06/09",5);
insert into estoque(codigo,quantidade,data_atualizacao,automovel_codigo)
values(null,3,"2015/09/07",2);
insert into estoque(codigo,quantidade,data_atualizacao,automovel_codigo)
values(null,4,"2015/11/09",1);
insert into estoque(codigo,quantidade,data_atualizacao,automovel_codigo)
values(null,5,"2015/10/11",2);

insert into compra(codigo,preco,n_parcela,data_compra,desconto,cliente_codigo,automovel_codigo,forma_de_pagamento_codigo,vendedor_codigo)
values(null,22.000,20,'2015/10/11',0,2,2,2,1);
insert into compra(codigo,preco,n_parcela,data_compra,desconto,cliente_codigo,automovel_codigo,forma_de_pagamento_codigo,vendedor_codigo)
values(null,231.000,0,'2015/09/07',5.000,5,1,1,2);
insert into compra(codigo,preco,n_parcela,data_compra,desconto,cliente_codigo,automovel_codigo,forma_de_pagamento_codigo,vendedor_codigo)
values(null,110.000,10,'2015/07/23',0,1,3,5,3);
insert into compra(codigo,preco,n_parcela,data_compra,desconto,cliente_codigo,automovel_codigo,forma_de_pagamento_codigo,vendedor_codigo)
values(null,16.900,10,'2014/11/09',0,4,4,3,4);
insert into compra(codigo,preco,n_parcela,data_compra,desconto,cliente_codigo,automovel_codigo,forma_de_pagamento_codigo,vendedor_codigo)
values(null,82.790,0,'2015/06/09',4.000,5,5,1,5);





use bhcar;
SELECT login.usuario, login.senha, nivel.descricao FROM login JOIN nivel ON nivel.codigo = login.nivel_codigo WHERE login.usuario = 'Joao' AND nivel.descricao = 'Funcionário';



SELECT * FROM login JOIN nivel ON nivel.codigo = login.nivel_codigo  WHERE nivel.descricao = @nivel;






SELECT automovel.codigo, automovel.chassi, automovel.preco, automovel.cor, automovel.cambio, automovel.versao, automovel.portas, automovel.motor, automovel.quilometragem, automovel.transmissao, automovel.combustivel, automovel.acessorios, automovel.ano, automovel.modelo, fornecedor.nome 
FROM automovel JOIN fornecedor 
ON fornecedor.codigo = fornecedor_codigo 
ORDER BY chassi DESC;

SELECT estoque.codigo, estoque.quantidade, estoque.data_atualizacao, automovel.modelo FROM estoque JOIN automovel ORDER BY estoque.data_atualizacao DESC;

















































SELECT compra.codigo, compra.preco, compra.n_parcela, compra.data_compra, compra.desconto, cliente.nome, automovel.modelo, forma_de_pagamento.descricao, vendedor.nome 
FROM compra JOIN cliente JOIN automovel JOIN forma_de_pagamento JOIN vendedor  
ON cliente.codigo = compra.cliente_codigo AND
ON automovel.codigo = automovel_codigo AND
ON forma_de_pagamento.codigo = compra.forma_de_pagamento_codigo AND
ON vendedor.codigo = compra.vendedor_codigo AND
ORDER BY compra.data_compra DESC;

















