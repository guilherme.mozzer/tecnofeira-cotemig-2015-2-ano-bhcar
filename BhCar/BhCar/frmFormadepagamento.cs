﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BhCar
{
    public partial class frmFormadepagamento : Form
    {
        public frmFormadepagamento()
        {
            InitializeComponent();
        }
        conexaobd bd = new conexaobd();
        private void bntCadastrar_Click(object sender, EventArgs e)
        {
            string inserir;
            if (txtdescricao.Text == "")
            {
                MessageBox.Show("Preencha o Campo Tipo de Pagamento", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtdescricao.Focus();
            }
            else
            {
                txtcodigo.Clear();
                inserir = "insert into forma_de_pagamento(codigo,descricao)  values(null, '" + txtdescricao.Text + "')";

                bd.executarcomandos(inserir);

                MessageBox.Show("Cadastro Realizado com sucesso!");
                txtdescricao.Clear();
                txtdescricao.Focus();
            }
        }

        private void bntPesquisar_Click(object sender, EventArgs e)
        {
            string sel = "SELECT * FROM forma_de_pagamento";
            DataTable dt = bd.executarconsulta(sel);
            dtgPagamento.DataSource = dt;
        }

        private void bntAlterar_Click(object sender, EventArgs e)
        {
            if (txtdescricao.Text == "")
            {
                MessageBox.Show("Preencha o Campo Tipo de Pagamento", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtdescricao.Focus();
            }
            else
            {
                string alterar;
                alterar = "update forma_de_pagamento set  descricao = '" + txtdescricao.Text + "' where codigo = " + long.Parse(txtcodigo.Text) ;
                bd.executarcomandos(alterar);
                MessageBox.Show("Cadastro Alterado com sucesso!");
                txtcodigo.Clear();
                txtdescricao.Clear();
                txtdescricao.Focus();
            }

        }

        private void dtgPagamento_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtcodigo.Text = dtgPagamento.Rows[e.RowIndex].Cells["codigo"].Value.ToString();
            txtdescricao.Text = dtgPagamento.Rows[e.RowIndex].Cells["descricao"].Value.ToString();
        }

        private void bntExcluir_Click(object sender, EventArgs e)
        {
            if (txtcodigo.Text == "")
            {
                MessageBox.Show("Selecione o Campo", "Aviso",
             MessageBoxButtons.OK, MessageBoxIcon.Warning);
                string sel = "SELECT * FROM forma_de_pagamento";
                DataTable dt = bd.executarconsulta(sel);
                dtgPagamento.DataSource = dt;
            }
            else if (MessageBox.Show("Tem certeza que deseja Excluir o cadastro?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                    string excluir = "delete from forma_de_pagamento where codigo = '" + long.Parse(txtcodigo.Text) + "'";
                    bd.executarcomandos(excluir);
                    MessageBox.Show("Cadastro Excluido com sucesso!");
                    txtcodigo.Clear();
                    txtdescricao.Clear();
                    txtdescricao.Focus();
            }
        }

        private void bntVoltar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
