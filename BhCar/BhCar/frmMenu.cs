﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace BhCar
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }
        conexaobd bd = new conexaobd();
      
        private void bntSair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja Sair? \n * Voltará para tela de Login!", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Close();
            }
        }
        private void bntAutomovel_Click(object sender, EventArgs e)
        {
            frmAutomovel tela = new frmAutomovel();
            tela.ShowDialog();
        }

        private void bntCliente_Click(object sender, EventArgs e)
        {
            frmCliente tela = new frmCliente();
            tela.ShowDialog();
        }

        private void bntFornecedor_Click(object sender, EventArgs e)
        {
                frmFornecedor tela = new frmFornecedor();
                tela.ShowDialog();
        }

        private void bntFormapagamento_Click(object sender, EventArgs e)
        {
                frmFormadepagamento tela = new frmFormadepagamento();
                tela.ShowDialog();

        }

        private void bntCompra_Click(object sender, EventArgs e)
        {
            frmCompra tela = new frmCompra();
            tela.ShowDialog();
        }

        private void bntEstoque_Click(object sender, EventArgs e)
        {
            string query = "SELECT login.codigo, login.usuario, login.senha, nivel.descricao FROM login JOIN nivel ON nivel.codigo = login.nivel_codigo  WHERE login.usuario = '" + dadosdelogin.Usuario + "' AND nivel.descricao = @nivel";
            conexaobd conexao = new conexaobd();
            conexao.ConectarBD();

            MySqlCommand cmd = new MySqlCommand(query, conexao.con);

            cmd.Parameters.AddWithValue("@nivel", "Funcionário");

            MySqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                MessageBox.Show("Acesso negado!", "Acesso Restrito",
             MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                frmEstoque tela = new frmEstoque();
                tela.ShowDialog();
            }
        }

        private void bntVendedor_Click(object sender, EventArgs e)
        {string query = "SELECT login.codigo, login.usuario, login.senha, nivel.descricao FROM login JOIN nivel ON nivel.codigo = login.nivel_codigo  WHERE login.usuario = '" + dadosdelogin.Usuario +"' AND nivel.descricao = @nivel";
            conexaobd conexao = new conexaobd();
            conexao.ConectarBD();

            MySqlCommand cmd = new MySqlCommand(query, conexao.con);

            cmd.Parameters.AddWithValue("@nivel","Funcionário");

            MySqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                MessageBox.Show("Acesso negado!", "Acesso Restrito",
             MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                frmVendedor tela = new frmVendedor();
            tela.ShowDialog();
          }
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {
            txtNome.Text = dadosdelogin.Nivel;
            txtNome.Text += " " + dadosdelogin.Usuario;
            string query = "SELECT login.codigo, login.usuario, login.senha, nivel.descricao FROM login JOIN nivel ON nivel.codigo = login.nivel_codigo  WHERE login.usuario = '" + dadosdelogin.Usuario + "' AND nivel.descricao = @nivel";
            conexaobd conexao = new conexaobd();
            conexao.ConectarBD();

            MySqlCommand cmd = new MySqlCommand(query, conexao.con);

            cmd.Parameters.AddWithValue("@nivel", "Funcionário");

            MySqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                bntFornecedor.Visible = false;
                bntFormapagamento.Visible = false;
                bntEstoque.Visible = false;
                bntVendedor.Visible = false;
                bntSair.Location = new Point(359,315);
            }
            else if(dadosdelogin.Nivel == "Gerente")
            {
                bntFornecedor.Visible = false;
                bntFormapagamento.Visible = false;
                bntVendedor.Visible = false;
                bntSair.Location = new Point(359, 371);
            }

        }
    }
}
