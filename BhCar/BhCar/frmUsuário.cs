﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace BhCar
{
    public partial class frmUsuário : Form
    {
        public frmUsuário()
        {
            InitializeComponent();
        }
        conexaobd bd = new conexaobd();
        private void bntCancelar_Click(object sender, EventArgs e)
        {
            //Sair
            Close();
        }

        private void ckbSenha_CheckedChanged(object sender, EventArgs e)
        {
            // Mostrar Senha
            if (ckbSenha.Checked == true)
            {
                txtSenha.UseSystemPasswordChar = false;
            }
            if (ckbSenha.Checked == false)
            {
                txtSenha.UseSystemPasswordChar = true;
            }
        }

        private void frmUsuário_Load(object sender, EventArgs e)
        {
            // preenche o combobox
            string sel = "SELECT codigo, descricao FROM nivel ORDER BY descricao ASC";
            DataTable dt = bd.executarconsulta(sel);
            cmbNivel.DisplayMember = "descricao";
            cmbNivel.ValueMember = "codigo";
            cmbNivel.DataSource = dt;

            //pede a senha de administrador

        }

        private void bntCadastrar_Click(object sender, EventArgs e)
        { //verifica se tem acesso
            string query = "SELECT * FROM login WHERE usuario = @usuario";
            conexaobd conexao = new conexaobd();
            conexao.ConectarBD();

            MySqlCommand cmd = new MySqlCommand(query, conexao.con);

            cmd.Parameters.AddWithValue("@usuario", txtUsuario.Text);

            MySqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                MessageBox.Show("Nome de Usuário já Cadastrado!", "Acesso Restrito",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtUsuario.Clear();
                txtUsuario.Focus();
            }
            else
            {
                //faz a validacão dos campos
                if (txtUsuario.Text.Length < 3)
                {
                    MessageBox.Show("O Usuário tem que ter pelo menos 3 (tres) carácteres", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtUsuario.Focus();
                }
                else if (txtSenha.Text.Length < 8)
                {
                    MessageBox.Show("Senha tem que ter pelo menos 8(oito) caracteres", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtSenha.Focus();
                }
                else if (cmbNivel.Text == "")
                {
                    MessageBox.Show("Selecione o Nível", "Aviso",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    cmbNivel.Focus();
                }
                else
                {
                    //isere no banco de dados
                    string inserir;
                    inserir = " INSERT INTO login (codigo, usuario, senha, nivel_codigo) VALUES(null, '" + txtUsuario.Text + "', '" + txtSenha.Text + "', " + cmbNivel.SelectedValue + ")";
                    bd.executarcomandos(inserir);
                    MessageBox.Show("Cadastro Realizado com sucesso!");
                    txtUsuario.Clear();
                    txtSenha.Clear();
                    Close();
                }
            }
        }
    }
}
