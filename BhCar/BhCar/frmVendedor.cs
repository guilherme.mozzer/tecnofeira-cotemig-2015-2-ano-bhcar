﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BhCar
{
    public partial class frmVendedor : Form
    {
        public frmVendedor()
        {
            InitializeComponent();
        }
        conexaobd bd = new conexaobd();

        private void bntVoltar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                switch (txtTelefone.TextLength)
                {
                    case 0:
                        txtTelefone.Text = "(";
                        txtTelefone.SelectionStart = 2;
                        break;
                    case 3:
                        txtTelefone.Text = txtTelefone.Text + ")";
                        txtTelefone.SelectionStart = 4;
                        break;
                }
            }
        }

        private void dtgvendedor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtCodigo.Text = dtgvendedor.Rows[e.RowIndex].Cells["codigo"].Value.ToString();
            txtNome.Text = dtgvendedor.Rows[e.RowIndex].Cells["nome"].Value.ToString();
            txtEmail.Text = dtgvendedor.Rows[e.RowIndex].Cells["email"].Value.ToString();
            txtTelefone.Text = dtgvendedor.Rows[e.RowIndex].Cells["telefone"].Value.ToString();
        }

        private void bntCadastrar_Click(object sender, EventArgs e)
        {
            if (txtNome.Text == "")
            {
                MessageBox.Show("Preencha o Campo Nome", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNome.Focus();
            }
            else if (rbnM.Checked == false && rbnF.Checked == false)
            {
                MessageBox.Show("O Campo Sexo deve ser marcado!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                rbnM.Focus();
            }
            else if ((!txtEmail.Text.Contains("@") || !txtEmail.Text.Contains(".")) || txtEmail.Text.Length == 0)
            {
                MessageBox.Show("O Campo Email é Inválido!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.Focus();
            }
            else if (txtTelefone.Text.Length != 12 || txtTelefone.Text.Length != 13)
            {
                MessageBox.Show("O Campo Telefone é Inválido", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTelefone.Focus();
            }
            else
            {
                txtCodigo.Clear();
                string inserir;
                string sex = rbnM.Checked ? "M" : "F";
                inserir = "insert into vendedor (codigo,nome,sexo, email,telefone) values (NULL,'" + txtNome.Text + "', '" + sex + "','" + txtEmail.Text + "', '" + txtTelefone.Text + "')";

                bd.executarcomandos(inserir);
                MessageBox.Show("Cadastro Realizado com sucesso!");
                txtCodigo.Clear();
                txtNome.Clear();
                txtEmail.Clear();
                txtTelefone.Clear();
                txtNome.Focus();
            }
            
        }

        private void bntPesquisar_Click(object sender, EventArgs e)
        {
            string sel = "SELECT *FROM vendedor order by nome";
            DataTable dt = bd.executarconsulta(sel);
            dtgvendedor.DataSource = dt;
        }

        private void bntAlterar_Click(object sender, EventArgs e)
        {
            if (txtNome.Text == "")
            {
                MessageBox.Show("Preencha o Campo Nome", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtNome.Focus();
            }
            else if (rbnM.Checked == false && rbnF.Checked == false)
            {
                MessageBox.Show("O Campo Sexo deve ser marcado!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                rbnM.Focus();
            }
            else if ((!txtEmail.Text.Contains("@") || !txtEmail.Text.Contains(".")) || txtEmail.Text.Length == 0)
            {
                MessageBox.Show("O Campo Email é Inválido!", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtEmail.Focus();
            }
            else if (txtTelefone.Text.Length != 13)
            {
                MessageBox.Show("O Campo Telefone é Inválido", "Aviso",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTelefone.Focus();
            }
            else
            {
                string alterar;
                string sex = rbnM.Checked ? "M" : "F";
                alterar = "update vendedor set nome = '" + txtNome.Text + "', sexo = '" + sex + "', email ='" + txtEmail.Text + "', telefone ='" + txtTelefone.Text + "'where codigo = " + long.Parse(txtCodigo.Text) ;
                bd.executarcomandos(alterar);
                MessageBox.Show("Cadastro Alterado com sucesso!");
                txtCodigo.Clear();
                txtNome.Clear();
                txtEmail.Clear();
                txtTelefone.Clear();
                txtNome.Focus();
            }
        }

        private void bntExcluir_Click(object sender, EventArgs e)
        {
            if (txtCodigo.Text == "")
            {
                MessageBox.Show("Selecione o Campo", "Aviso",
             MessageBoxButtons.OK, MessageBoxIcon.Warning);
                string sel = "SELECT *FROM vendedor order by nome";
                DataTable dt = bd.executarconsulta(sel);
                dtgvendedor.DataSource = dt;

            }
            else if (MessageBox.Show("Tem certeza que deseja Excluir o cadastro?", "Confirmação", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                    string excluir;
                    excluir = "delete from vendedor where codigo = " + long.Parse(txtCodigo.Text) + "";
                    bd.executarcomandos(excluir);
                    MessageBox.Show("Cadastro Excluido com sucesso!");
                    txtCodigo.Clear();
                    txtNome.Clear();
                    txtEmail.Clear();
                    txtTelefone.Clear();
                    txtNome.Focus();
            }
        }
    }
}
