﻿namespace BhCar
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.lblUsuário = new System.Windows.Forms.Label();
            this.lblSenha = new System.Windows.Forms.Label();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.bnttExtrar = new System.Windows.Forms.Button();
            this.bntCancelar = new System.Windows.Forms.Button();
            this.lblCriarUsuario = new System.Windows.Forms.LinkLabel();
            this.PctCabecalho = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbNivel = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.PctCabecalho)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUsuário
            // 
            this.lblUsuário.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblUsuário.AutoSize = true;
            this.lblUsuário.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuário.Location = new System.Drawing.Point(27, 167);
            this.lblUsuário.Name = "lblUsuário";
            this.lblUsuário.Size = new System.Drawing.Size(58, 16);
            this.lblUsuário.TabIndex = 0;
            this.lblUsuário.Text = "Usuário:";
            // 
            // lblSenha
            // 
            this.lblSenha.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblSenha.AutoSize = true;
            this.lblSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSenha.Location = new System.Drawing.Point(27, 193);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(50, 16);
            this.lblSenha.TabIndex = 1;
            this.lblSenha.Text = "Senha:";
            // 
            // txtUsuario
            // 
            this.txtUsuario.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtUsuario.Location = new System.Drawing.Point(91, 166);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(168, 20);
            this.txtUsuario.TabIndex = 0;
            // 
            // txtSenha
            // 
            this.txtSenha.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.txtSenha.Location = new System.Drawing.Point(91, 192);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.PasswordChar = '*';
            this.txtSenha.Size = new System.Drawing.Size(168, 20);
            this.txtSenha.TabIndex = 1;
            this.txtSenha.UseSystemPasswordChar = true;
            // 
            // bnttExtrar
            // 
            this.bnttExtrar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bnttExtrar.Location = new System.Drawing.Point(91, 293);
            this.bnttExtrar.Name = "bnttExtrar";
            this.bnttExtrar.Size = new System.Drawing.Size(81, 23);
            this.bnttExtrar.TabIndex = 3;
            this.bnttExtrar.Text = "Entrar";
            this.bnttExtrar.UseVisualStyleBackColor = true;
            this.bnttExtrar.Click += new System.EventHandler(this.bnttExtrar_Click);
            // 
            // bntCancelar
            // 
            this.bntCancelar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bntCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bntCancelar.Location = new System.Drawing.Point(178, 293);
            this.bntCancelar.Name = "bntCancelar";
            this.bntCancelar.Size = new System.Drawing.Size(81, 23);
            this.bntCancelar.TabIndex = 4;
            this.bntCancelar.Text = "Cancelar";
            this.bntCancelar.UseVisualStyleBackColor = true;
            this.bntCancelar.Click += new System.EventHandler(this.bntCancelar_Click);
            // 
            // lblCriarUsuario
            // 
            this.lblCriarUsuario.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblCriarUsuario.AutoSize = true;
            this.lblCriarUsuario.LinkColor = System.Drawing.Color.Black;
            this.lblCriarUsuario.Location = new System.Drawing.Point(163, 242);
            this.lblCriarUsuario.Name = "lblCriarUsuario";
            this.lblCriarUsuario.Size = new System.Drawing.Size(96, 13);
            this.lblCriarUsuario.TabIndex = 5;
            this.lblCriarUsuario.TabStop = true;
            this.lblCriarUsuario.Text = "Criar Novo Usuário";
            this.lblCriarUsuario.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblCriarUsuario_LinkClicked);
            // 
            // PctCabecalho
            // 
            this.PctCabecalho.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PctCabecalho.Cursor = System.Windows.Forms.Cursors.Default;
            this.PctCabecalho.Enabled = false;
            this.PctCabecalho.Image = ((System.Drawing.Image)(resources.GetObject("PctCabecalho.Image")));
            this.PctCabecalho.ImageLocation = "";
            this.PctCabecalho.Location = new System.Drawing.Point(-1, 2);
            this.PctCabecalho.Name = "PctCabecalho";
            this.PctCabecalho.Size = new System.Drawing.Size(312, 123);
            this.PctCabecalho.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PctCabecalho.TabIndex = 7;
            this.PctCabecalho.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(28, 219);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 16);
            this.label1.TabIndex = 11;
            this.label1.Text = "Nível:";
            // 
            // cmbNivel
            // 
            this.cmbNivel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cmbNivel.FormattingEnabled = true;
            this.cmbNivel.Items.AddRange(new object[] {
            "Administrador",
            "Funcionário",
            "Gerente"});
            this.cmbNivel.Location = new System.Drawing.Point(91, 218);
            this.cmbNivel.Name = "cmbNivel";
            this.cmbNivel.Size = new System.Drawing.Size(168, 21);
            this.cmbNivel.TabIndex = 2;
            // 
            // frmLogin
            // 
            this.AcceptButton = this.bnttExtrar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.CancelButton = this.bntCancelar;
            this.ClientSize = new System.Drawing.Size(311, 356);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbNivel);
            this.Controls.Add(this.PctCabecalho);
            this.Controls.Add(this.lblCriarUsuario);
            this.Controls.Add(this.bntCancelar);
            this.Controls.Add(this.bnttExtrar);
            this.Controls.Add(this.txtSenha);
            this.Controls.Add(this.txtUsuario);
            this.Controls.Add(this.lblSenha);
            this.Controls.Add(this.lblUsuário);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login de Usuário";
            ((System.ComponentModel.ISupportInitialize)(this.PctCabecalho)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUsuário;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Button bnttExtrar;
        private System.Windows.Forms.Button bntCancelar;
        private System.Windows.Forms.LinkLabel lblCriarUsuario;
        private System.Windows.Forms.PictureBox PctCabecalho;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbNivel;
    }
}