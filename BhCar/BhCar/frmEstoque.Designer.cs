﻿namespace BhCar
{
    partial class frmEstoque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEstoque));
            this.pictureBoxCabecalho = new System.Windows.Forms.PictureBox();
            this.bntVoltar = new System.Windows.Forms.PictureBox();
            this.bntPesquisar = new System.Windows.Forms.PictureBox();
            this.bntAlterar = new System.Windows.Forms.PictureBox();
            this.bntExcluir = new System.Windows.Forms.PictureBox();
            this.bntCadastrar = new System.Windows.Forms.PictureBox();
            this.lblObrigatorio = new System.Windows.Forms.Label();
            this.dtgEstoque = new System.Windows.Forms.DataGridView();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtQuantidade = new System.Windows.Forms.TextBox();
            this.lblquantidade = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbAutomovel = new System.Windows.Forms.ComboBox();
            this.txtData = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCabecalho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVoltar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntPesquisar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAlterar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntExcluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCadastrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEstoque)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxCabecalho
            // 
            this.pictureBoxCabecalho.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCabecalho.Image")));
            this.pictureBoxCabecalho.Location = new System.Drawing.Point(0, 1);
            this.pictureBoxCabecalho.Name = "pictureBoxCabecalho";
            this.pictureBoxCabecalho.Size = new System.Drawing.Size(637, 88);
            this.pictureBoxCabecalho.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCabecalho.TabIndex = 5;
            this.pictureBoxCabecalho.TabStop = false;
            // 
            // bntVoltar
            // 
            this.bntVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntVoltar.Image = ((System.Drawing.Image)(resources.GetObject("bntVoltar.Image")));
            this.bntVoltar.Location = new System.Drawing.Point(475, 434);
            this.bntVoltar.Name = "bntVoltar";
            this.bntVoltar.Size = new System.Drawing.Size(150, 40);
            this.bntVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntVoltar.TabIndex = 235;
            this.bntVoltar.TabStop = false;
            this.bntVoltar.Click += new System.EventHandler(this.bntVoltar_Click);
            // 
            // bntPesquisar
            // 
            this.bntPesquisar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("bntPesquisar.Image")));
            this.bntPesquisar.Location = new System.Drawing.Point(163, 388);
            this.bntPesquisar.Name = "bntPesquisar";
            this.bntPesquisar.Size = new System.Drawing.Size(150, 40);
            this.bntPesquisar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntPesquisar.TabIndex = 234;
            this.bntPesquisar.TabStop = false;
            this.bntPesquisar.Click += new System.EventHandler(this.bntPesquisar_Click);
            // 
            // bntAlterar
            // 
            this.bntAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntAlterar.Image = ((System.Drawing.Image)(resources.GetObject("bntAlterar.Image")));
            this.bntAlterar.Location = new System.Drawing.Point(319, 388);
            this.bntAlterar.Name = "bntAlterar";
            this.bntAlterar.Size = new System.Drawing.Size(150, 40);
            this.bntAlterar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntAlterar.TabIndex = 233;
            this.bntAlterar.TabStop = false;
            this.bntAlterar.Click += new System.EventHandler(this.bntAlterar_Click);
            // 
            // bntExcluir
            // 
            this.bntExcluir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntExcluir.Image = ((System.Drawing.Image)(resources.GetObject("bntExcluir.Image")));
            this.bntExcluir.Location = new System.Drawing.Point(475, 388);
            this.bntExcluir.Name = "bntExcluir";
            this.bntExcluir.Size = new System.Drawing.Size(150, 40);
            this.bntExcluir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntExcluir.TabIndex = 232;
            this.bntExcluir.TabStop = false;
            this.bntExcluir.Click += new System.EventHandler(this.bntExcluir_Click);
            // 
            // bntCadastrar
            // 
            this.bntCadastrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntCadastrar.Image = ((System.Drawing.Image)(resources.GetObject("bntCadastrar.Image")));
            this.bntCadastrar.Location = new System.Drawing.Point(7, 388);
            this.bntCadastrar.Name = "bntCadastrar";
            this.bntCadastrar.Size = new System.Drawing.Size(150, 40);
            this.bntCadastrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntCadastrar.TabIndex = 231;
            this.bntCadastrar.TabStop = false;
            this.bntCadastrar.Click += new System.EventHandler(this.bntCadastrar_Click);
            // 
            // lblObrigatorio
            // 
            this.lblObrigatorio.AutoSize = true;
            this.lblObrigatorio.Location = new System.Drawing.Point(509, 156);
            this.lblObrigatorio.Name = "lblObrigatorio";
            this.lblObrigatorio.Size = new System.Drawing.Size(111, 13);
            this.lblObrigatorio.TabIndex = 230;
            this.lblObrigatorio.Text = "* Campos Obrigatórios";
            // 
            // dtgEstoque
            // 
            this.dtgEstoque.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dtgEstoque.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgEstoque.Location = new System.Drawing.Point(7, 172);
            this.dtgEstoque.Name = "dtgEstoque";
            this.dtgEstoque.Size = new System.Drawing.Size(618, 210);
            this.dtgEstoque.TabIndex = 4;
            this.dtgEstoque.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgEstoque_CellContentClick);
            // 
            // txtCodigo
            // 
            this.txtCodigo.Enabled = false;
            this.txtCodigo.Location = new System.Drawing.Point(58, 95);
            this.txtCodigo.MaxLength = 11;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(200, 20);
            this.txtCodigo.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 236;
            this.label2.Text = "Codigo:";
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.Location = new System.Drawing.Point(367, 95);
            this.txtQuantidade.MaxLength = 11;
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.Size = new System.Drawing.Size(258, 20);
            this.txtQuantidade.TabIndex = 1;
            // 
            // lblquantidade
            // 
            this.lblquantidade.AutoSize = true;
            this.lblquantidade.Location = new System.Drawing.Point(291, 98);
            this.lblquantidade.Name = "lblquantidade";
            this.lblquantidade.Size = new System.Drawing.Size(70, 13);
            this.lblquantidade.TabIndex = 238;
            this.lblquantidade.Text = "* quantidade:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 240;
            this.label1.Text = "* data de atualização:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(295, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 242;
            this.label3.Text = "* automóvel:";
            // 
            // cmbAutomovel
            // 
            this.cmbAutomovel.FormattingEnabled = true;
            this.cmbAutomovel.Location = new System.Drawing.Point(367, 121);
            this.cmbAutomovel.Name = "cmbAutomovel";
            this.cmbAutomovel.Size = new System.Drawing.Size(258, 21);
            this.cmbAutomovel.TabIndex = 3;
            // 
            // txtData
            // 
            this.txtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtData.Location = new System.Drawing.Point(125, 121);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(133, 20);
            this.txtData.TabIndex = 243;
            // 
            // frmEstoque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 489);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.cmbAutomovel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtQuantidade);
            this.Controls.Add(this.lblquantidade);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bntVoltar);
            this.Controls.Add(this.bntPesquisar);
            this.Controls.Add(this.bntAlterar);
            this.Controls.Add(this.bntExcluir);
            this.Controls.Add(this.bntCadastrar);
            this.Controls.Add(this.lblObrigatorio);
            this.Controls.Add(this.dtgEstoque);
            this.Controls.Add(this.pictureBoxCabecalho);
            this.MaximizeBox = false;
            this.Name = "frmEstoque";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estoque de Automóveis";
            this.Load += new System.EventHandler(this.frmEstoque_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCabecalho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVoltar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntPesquisar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAlterar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntExcluir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCadastrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEstoque)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxCabecalho;
        private System.Windows.Forms.PictureBox bntVoltar;
        private System.Windows.Forms.PictureBox bntPesquisar;
        private System.Windows.Forms.PictureBox bntAlterar;
        private System.Windows.Forms.PictureBox bntExcluir;
        private System.Windows.Forms.PictureBox bntCadastrar;
        private System.Windows.Forms.Label lblObrigatorio;
        private System.Windows.Forms.DataGridView dtgEstoque;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtQuantidade;
        private System.Windows.Forms.Label lblquantidade;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbAutomovel;
        private System.Windows.Forms.DateTimePicker txtData;
    }
}