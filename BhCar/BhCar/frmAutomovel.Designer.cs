﻿namespace BhCar
{
    partial class frmAutomovel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutomovel));
            this.pictureBoxCabecalho = new System.Windows.Forms.PictureBox();
            this.cmbFornecedor = new System.Windows.Forms.ComboBox();
            this.txtcodigo = new System.Windows.Forms.TextBox();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtpreco = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txttransmissao = new System.Windows.Forms.TextBox();
            this.txtcombustivel = new System.Windows.Forms.TextBox();
            this.txtquilometragem = new System.Windows.Forms.TextBox();
            this.txtversao = new System.Windows.Forms.TextBox();
            this.txtcambio = new System.Windows.Forms.TextBox();
            this.txtmotor = new System.Windows.Forms.TextBox();
            this.txtportas = new System.Windows.Forms.TextBox();
            this.txtcor = new System.Windows.Forms.TextBox();
            this.txtano = new System.Windows.Forms.TextBox();
            this.txtmodelo = new System.Windows.Forms.TextBox();
            this.txtchassi = new System.Windows.Forms.TextBox();
            this.lblcombustivel = new System.Windows.Forms.Label();
            this.lblcambio = new System.Windows.Forms.Label();
            this.lblversao = new System.Windows.Forms.Label();
            this.lblportas = new System.Windows.Forms.Label();
            this.lblmotor = new System.Windows.Forms.Label();
            this.lblkm = new System.Windows.Forms.Label();
            this.lbltransm = new System.Windows.Forms.Label();
            this.lblano = new System.Windows.Forms.Label();
            this.lblchassi = new System.Windows.Forms.Label();
            this.lblcor = new System.Windows.Forms.Label();
            this.lblmodelo = new System.Windows.Forms.Label();
            this.bntVoltar = new System.Windows.Forms.PictureBox();
            this.bntPesquisar = new System.Windows.Forms.PictureBox();
            this.bntAlterar = new System.Windows.Forms.PictureBox();
            this.bntExcluir = new System.Windows.Forms.PictureBox();
            this.bntCadastrar = new System.Windows.Forms.PictureBox();
            this.lblObrigatorio = new System.Windows.Forms.Label();
            this.dtgautomovel = new System.Windows.Forms.DataGridView();
            this.gpb = new System.Windows.Forms.GroupBox();
            this.ckbRoda = new System.Windows.Forms.CheckBox();
            this.ckbBanco = new System.Windows.Forms.CheckBox();
            this.ckbMultimidia = new System.Windows.Forms.CheckBox();
            this.ckbGps = new System.Windows.Forms.CheckBox();
            this.ckbRadio = new System.Windows.Forms.CheckBox();
            this.ckbOrganizador = new System.Windows.Forms.CheckBox();
            this.ckbAdesivo = new System.Windows.Forms.CheckBox();
            this.ckbFarolneblina = new System.Windows.Forms.CheckBox();
            this.ckbVidroele = new System.Windows.Forms.CheckBox();
            this.ckbArCondicionado = new System.Windows.Forms.CheckBox();
            this.ckbSensor = new System.Windows.Forms.CheckBox();
            this.ckbOutros = new System.Windows.Forms.CheckBox();
            this.txtOutros = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCabecalho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVoltar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntPesquisar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAlterar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntExcluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCadastrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgautomovel)).BeginInit();
            this.gpb.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxCabecalho
            // 
            this.pictureBoxCabecalho.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxCabecalho.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCabecalho.Image")));
            this.pictureBoxCabecalho.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxCabecalho.Name = "pictureBoxCabecalho";
            this.pictureBoxCabecalho.Size = new System.Drawing.Size(643, 78);
            this.pictureBoxCabecalho.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCabecalho.TabIndex = 0;
            this.pictureBoxCabecalho.TabStop = false;
            // 
            // cmbFornecedor
            // 
            this.cmbFornecedor.FormattingEnabled = true;
            this.cmbFornecedor.Location = new System.Drawing.Point(85, 240);
            this.cmbFornecedor.Name = "cmbFornecedor";
            this.cmbFornecedor.Size = new System.Drawing.Size(204, 21);
            this.cmbFornecedor.TabIndex = 13;
            // 
            // txtcodigo
            // 
            this.txtcodigo.Enabled = false;
            this.txtcodigo.Location = new System.Drawing.Point(61, 84);
            this.txtcodigo.MaxLength = 11;
            this.txtcodigo.Name = "txtcodigo";
            this.txtcodigo.Size = new System.Drawing.Size(228, 20);
            this.txtcodigo.TabIndex = 0;
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(12, 87);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(43, 13);
            this.lblCodigo.TabIndex = 198;
            this.lblCodigo.Text = "Código:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 243);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 197;
            this.label2.Text = "* Fornecedor :";
            // 
            // txtpreco
            // 
            this.txtpreco.Location = new System.Drawing.Point(61, 214);
            this.txtpreco.MaxLength = 20;
            this.txtpreco.Name = "txtpreco";
            this.txtpreco.Size = new System.Drawing.Size(228, 20);
            this.txtpreco.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 217);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 196;
            this.label1.Text = "* PREÇO :";
            // 
            // txttransmissao
            // 
            this.txttransmissao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txttransmissao.Location = new System.Drawing.Point(390, 136);
            this.txttransmissao.MaxLength = 50;
            this.txttransmissao.Name = "txttransmissao";
            this.txttransmissao.Size = new System.Drawing.Size(240, 20);
            this.txttransmissao.TabIndex = 5;
            // 
            // txtcombustivel
            // 
            this.txtcombustivel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtcombustivel.Location = new System.Drawing.Point(390, 214);
            this.txtcombustivel.MaxLength = 50;
            this.txtcombustivel.Name = "txtcombustivel";
            this.txtcombustivel.Size = new System.Drawing.Size(240, 20);
            this.txtcombustivel.TabIndex = 12;
            // 
            // txtquilometragem
            // 
            this.txtquilometragem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtquilometragem.Location = new System.Drawing.Point(390, 188);
            this.txtquilometragem.MaxLength = 20;
            this.txtquilometragem.Name = "txtquilometragem";
            this.txtquilometragem.Size = new System.Drawing.Size(240, 20);
            this.txtquilometragem.TabIndex = 10;
            // 
            // txtversao
            // 
            this.txtversao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtversao.Location = new System.Drawing.Point(390, 162);
            this.txtversao.MaxLength = 80;
            this.txtversao.Name = "txtversao";
            this.txtversao.Size = new System.Drawing.Size(240, 20);
            this.txtversao.TabIndex = 7;
            // 
            // txtcambio
            // 
            this.txtcambio.Location = new System.Drawing.Point(61, 162);
            this.txtcambio.MaxLength = 30;
            this.txtcambio.Name = "txtcambio";
            this.txtcambio.Size = new System.Drawing.Size(228, 20);
            this.txtcambio.TabIndex = 6;
            // 
            // txtmotor
            // 
            this.txtmotor.Location = new System.Drawing.Point(61, 136);
            this.txtmotor.MaxLength = 100;
            this.txtmotor.Name = "txtmotor";
            this.txtmotor.Size = new System.Drawing.Size(228, 20);
            this.txtmotor.TabIndex = 4;
            // 
            // txtportas
            // 
            this.txtportas.Location = new System.Drawing.Point(61, 188);
            this.txtportas.MaxLength = 1;
            this.txtportas.Name = "txtportas";
            this.txtportas.Size = new System.Drawing.Size(89, 20);
            this.txtportas.TabIndex = 8;
            // 
            // txtcor
            // 
            this.txtcor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtcor.Location = new System.Drawing.Point(390, 110);
            this.txtcor.MaxLength = 80;
            this.txtcor.Name = "txtcor";
            this.txtcor.Size = new System.Drawing.Size(240, 20);
            this.txtcor.TabIndex = 3;
            // 
            // txtano
            // 
            this.txtano.Location = new System.Drawing.Point(203, 188);
            this.txtano.MaxLength = 4;
            this.txtano.Name = "txtano";
            this.txtano.Size = new System.Drawing.Size(86, 20);
            this.txtano.TabIndex = 9;
            // 
            // txtmodelo
            // 
            this.txtmodelo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtmodelo.Location = new System.Drawing.Point(390, 84);
            this.txtmodelo.MaxLength = 100;
            this.txtmodelo.Name = "txtmodelo";
            this.txtmodelo.Size = new System.Drawing.Size(240, 20);
            this.txtmodelo.TabIndex = 1;
            // 
            // txtchassi
            // 
            this.txtchassi.Location = new System.Drawing.Point(61, 110);
            this.txtchassi.MaxLength = 14;
            this.txtchassi.Name = "txtchassi";
            this.txtchassi.Size = new System.Drawing.Size(228, 20);
            this.txtchassi.TabIndex = 2;
            // 
            // lblcombustivel
            // 
            this.lblcombustivel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblcombustivel.AutoSize = true;
            this.lblcombustivel.Location = new System.Drawing.Point(310, 217);
            this.lblcombustivel.Name = "lblcombustivel";
            this.lblcombustivel.Size = new System.Drawing.Size(74, 13);
            this.lblcombustivel.TabIndex = 194;
            this.lblcombustivel.Text = "* Combustivel:";
            // 
            // lblcambio
            // 
            this.lblcambio.AutoSize = true;
            this.lblcambio.Location = new System.Drawing.Point(3, 165);
            this.lblcambio.Name = "lblcambio";
            this.lblcambio.Size = new System.Drawing.Size(52, 13);
            this.lblcambio.TabIndex = 193;
            this.lblcambio.Text = "* Câmbio:";
            // 
            // lblversao
            // 
            this.lblversao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblversao.AutoSize = true;
            this.lblversao.Location = new System.Drawing.Point(331, 165);
            this.lblversao.Name = "lblversao";
            this.lblversao.Size = new System.Drawing.Size(53, 13);
            this.lblversao.TabIndex = 192;
            this.lblversao.Text = "* Versão :";
            // 
            // lblportas
            // 
            this.lblportas.AutoSize = true;
            this.lblportas.Location = new System.Drawing.Point(5, 191);
            this.lblportas.Name = "lblportas";
            this.lblportas.Size = new System.Drawing.Size(50, 13);
            this.lblportas.TabIndex = 191;
            this.lblportas.Text = "* Portas :";
            // 
            // lblmotor
            // 
            this.lblmotor.AutoSize = true;
            this.lblmotor.Location = new System.Drawing.Point(8, 139);
            this.lblmotor.Name = "lblmotor";
            this.lblmotor.Size = new System.Drawing.Size(47, 13);
            this.lblmotor.TabIndex = 190;
            this.lblmotor.Text = "* Motor :";
            // 
            // lblkm
            // 
            this.lblkm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblkm.AutoSize = true;
            this.lblkm.Location = new System.Drawing.Point(294, 191);
            this.lblkm.Name = "lblkm";
            this.lblkm.Size = new System.Drawing.Size(90, 13);
            this.lblkm.TabIndex = 189;
            this.lblkm.Text = "* Quilometragem :";
            // 
            // lbltransm
            // 
            this.lbltransm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbltransm.AutoSize = true;
            this.lbltransm.Location = new System.Drawing.Point(307, 140);
            this.lbltransm.Name = "lbltransm";
            this.lbltransm.Size = new System.Drawing.Size(76, 13);
            this.lbltransm.TabIndex = 188;
            this.lbltransm.Text = "* Transmissão:";
            // 
            // lblano
            // 
            this.lblano.AutoSize = true;
            this.lblano.Location = new System.Drawing.Point(158, 192);
            this.lblano.Name = "lblano";
            this.lblano.Size = new System.Drawing.Size(39, 13);
            this.lblano.TabIndex = 187;
            this.lblano.Text = "* Ano :";
            // 
            // lblchassi
            // 
            this.lblchassi.AutoSize = true;
            this.lblchassi.Location = new System.Drawing.Point(4, 113);
            this.lblchassi.Name = "lblchassi";
            this.lblchassi.Size = new System.Drawing.Size(51, 13);
            this.lblchassi.TabIndex = 186;
            this.lblchassi.Text = "* Chassi :";
            // 
            // lblcor
            // 
            this.lblcor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblcor.AutoSize = true;
            this.lblcor.Location = new System.Drawing.Point(348, 114);
            this.lblcor.Name = "lblcor";
            this.lblcor.Size = new System.Drawing.Size(36, 13);
            this.lblcor.TabIndex = 185;
            this.lblcor.Text = "* Cor :";
            // 
            // lblmodelo
            // 
            this.lblmodelo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblmodelo.AutoSize = true;
            this.lblmodelo.Location = new System.Drawing.Point(329, 87);
            this.lblmodelo.Name = "lblmodelo";
            this.lblmodelo.Size = new System.Drawing.Size(55, 13);
            this.lblmodelo.TabIndex = 184;
            this.lblmodelo.Text = "* Modelo :";
            // 
            // bntVoltar
            // 
            this.bntVoltar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntVoltar.Image = ((System.Drawing.Image)(resources.GetObject("bntVoltar.Image")));
            this.bntVoltar.Location = new System.Drawing.Point(480, 690);
            this.bntVoltar.Name = "bntVoltar";
            this.bntVoltar.Size = new System.Drawing.Size(150, 40);
            this.bntVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntVoltar.TabIndex = 221;
            this.bntVoltar.TabStop = false;
            this.bntVoltar.Click += new System.EventHandler(this.bntVoltar_Click);
            // 
            // bntPesquisar
            // 
            this.bntPesquisar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntPesquisar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("bntPesquisar.Image")));
            this.bntPesquisar.Location = new System.Drawing.Point(168, 644);
            this.bntPesquisar.Name = "bntPesquisar";
            this.bntPesquisar.Size = new System.Drawing.Size(150, 40);
            this.bntPesquisar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntPesquisar.TabIndex = 220;
            this.bntPesquisar.TabStop = false;
            this.bntPesquisar.Click += new System.EventHandler(this.bntPesquisar_Click);
            // 
            // bntAlterar
            // 
            this.bntAlterar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntAlterar.Image = ((System.Drawing.Image)(resources.GetObject("bntAlterar.Image")));
            this.bntAlterar.Location = new System.Drawing.Point(324, 644);
            this.bntAlterar.Name = "bntAlterar";
            this.bntAlterar.Size = new System.Drawing.Size(150, 40);
            this.bntAlterar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntAlterar.TabIndex = 219;
            this.bntAlterar.TabStop = false;
            this.bntAlterar.Click += new System.EventHandler(this.bntAlterar_Click);
            // 
            // bntExcluir
            // 
            this.bntExcluir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntExcluir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntExcluir.Image = ((System.Drawing.Image)(resources.GetObject("bntExcluir.Image")));
            this.bntExcluir.Location = new System.Drawing.Point(480, 644);
            this.bntExcluir.Name = "bntExcluir";
            this.bntExcluir.Size = new System.Drawing.Size(150, 40);
            this.bntExcluir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntExcluir.TabIndex = 218;
            this.bntExcluir.TabStop = false;
            this.bntExcluir.Click += new System.EventHandler(this.bntExcluir_Click);
            // 
            // bntCadastrar
            // 
            this.bntCadastrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bntCadastrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntCadastrar.Image = ((System.Drawing.Image)(resources.GetObject("bntCadastrar.Image")));
            this.bntCadastrar.Location = new System.Drawing.Point(12, 644);
            this.bntCadastrar.Name = "bntCadastrar";
            this.bntCadastrar.Size = new System.Drawing.Size(150, 40);
            this.bntCadastrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntCadastrar.TabIndex = 217;
            this.bntCadastrar.TabStop = false;
            this.bntCadastrar.Click += new System.EventHandler(this.bntCadastrar_Click);
            // 
            // lblObrigatorio
            // 
            this.lblObrigatorio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblObrigatorio.AutoSize = true;
            this.lblObrigatorio.Location = new System.Drawing.Point(520, 400);
            this.lblObrigatorio.Name = "lblObrigatorio";
            this.lblObrigatorio.Size = new System.Drawing.Size(111, 13);
            this.lblObrigatorio.TabIndex = 216;
            this.lblObrigatorio.Text = "* Campos Obrigatórios";
            // 
            // dtgautomovel
            // 
            this.dtgautomovel.AllowUserToAddRows = false;
            this.dtgautomovel.AllowUserToDeleteRows = false;
            this.dtgautomovel.AllowUserToOrderColumns = true;
            this.dtgautomovel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtgautomovel.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dtgautomovel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgautomovel.Location = new System.Drawing.Point(12, 416);
            this.dtgautomovel.Name = "dtgautomovel";
            this.dtgautomovel.ReadOnly = true;
            this.dtgautomovel.Size = new System.Drawing.Size(618, 222);
            this.dtgautomovel.TabIndex = 25;
            this.dtgautomovel.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgautomovel_CellContentClick);
            // 
            // gpb
            // 
            this.gpb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpb.Controls.Add(this.txtOutros);
            this.gpb.Controls.Add(this.ckbOutros);
            this.gpb.Controls.Add(this.ckbRoda);
            this.gpb.Controls.Add(this.ckbBanco);
            this.gpb.Controls.Add(this.ckbMultimidia);
            this.gpb.Controls.Add(this.ckbGps);
            this.gpb.Controls.Add(this.ckbRadio);
            this.gpb.Controls.Add(this.ckbOrganizador);
            this.gpb.Controls.Add(this.ckbAdesivo);
            this.gpb.Controls.Add(this.ckbFarolneblina);
            this.gpb.Controls.Add(this.ckbVidroele);
            this.gpb.Controls.Add(this.ckbArCondicionado);
            this.gpb.Controls.Add(this.ckbSensor);
            this.gpb.Location = new System.Drawing.Point(11, 267);
            this.gpb.Name = "gpb";
            this.gpb.Size = new System.Drawing.Size(619, 114);
            this.gpb.TabIndex = 222;
            this.gpb.TabStop = false;
            this.gpb.Text = "* Acessórios";
            // 
            // ckbRoda
            // 
            this.ckbRoda.AutoSize = true;
            this.ckbRoda.Location = new System.Drawing.Point(284, 65);
            this.ckbRoda.Name = "ckbRoda";
            this.ckbRoda.Size = new System.Drawing.Size(117, 17);
            this.ckbRoda.TabIndex = 22;
            this.ckbRoda.Text = "Roda de Liga Leve";
            this.ckbRoda.UseVisualStyleBackColor = true;
            // 
            // ckbBanco
            // 
            this.ckbBanco.AutoSize = true;
            this.ckbBanco.Location = new System.Drawing.Point(284, 42);
            this.ckbBanco.Name = "ckbBanco";
            this.ckbBanco.Size = new System.Drawing.Size(103, 17);
            this.ckbBanco.TabIndex = 21;
            this.ckbBanco.Text = "Banco de Couro";
            this.ckbBanco.UseVisualStyleBackColor = true;
            // 
            // ckbMultimidia
            // 
            this.ckbMultimidia.AutoSize = true;
            this.ckbMultimidia.Location = new System.Drawing.Point(284, 19);
            this.ckbMultimidia.Name = "ckbMultimidia";
            this.ckbMultimidia.Size = new System.Drawing.Size(74, 17);
            this.ckbMultimidia.TabIndex = 20;
            this.ckbMultimidia.Text = "Multimídia";
            this.ckbMultimidia.UseVisualStyleBackColor = true;
            // 
            // ckbGps
            // 
            this.ckbGps.AutoSize = true;
            this.ckbGps.Location = new System.Drawing.Point(425, 19);
            this.ckbGps.Name = "ckbGps";
            this.ckbGps.Size = new System.Drawing.Size(104, 17);
            this.ckbGps.TabIndex = 23;
            this.ckbGps.Text = "Navegador GPS";
            this.ckbGps.UseVisualStyleBackColor = true;
            // 
            // ckbRadio
            // 
            this.ckbRadio.AutoSize = true;
            this.ckbRadio.Location = new System.Drawing.Point(179, 65);
            this.ckbRadio.Name = "ckbRadio";
            this.ckbRadio.Size = new System.Drawing.Size(54, 17);
            this.ckbRadio.TabIndex = 19;
            this.ckbRadio.Text = "Rádio";
            this.ckbRadio.UseVisualStyleBackColor = true;
            // 
            // ckbOrganizador
            // 
            this.ckbOrganizador.AutoSize = true;
            this.ckbOrganizador.Location = new System.Drawing.Point(179, 42);
            this.ckbOrganizador.Name = "ckbOrganizador";
            this.ckbOrganizador.Size = new System.Drawing.Size(83, 17);
            this.ckbOrganizador.TabIndex = 18;
            this.ckbOrganizador.Text = "Organizador";
            this.ckbOrganizador.UseVisualStyleBackColor = true;
            // 
            // ckbAdesivo
            // 
            this.ckbAdesivo.AutoSize = true;
            this.ckbAdesivo.Location = new System.Drawing.Point(179, 19);
            this.ckbAdesivo.Name = "ckbAdesivo";
            this.ckbAdesivo.Size = new System.Drawing.Size(64, 17);
            this.ckbAdesivo.TabIndex = 17;
            this.ckbAdesivo.Text = "Adesivo";
            this.ckbAdesivo.UseVisualStyleBackColor = true;
            // 
            // ckbFarolneblina
            // 
            this.ckbFarolneblina.AutoSize = true;
            this.ckbFarolneblina.Location = new System.Drawing.Point(425, 42);
            this.ckbFarolneblina.Name = "ckbFarolneblina";
            this.ckbFarolneblina.Size = new System.Drawing.Size(103, 17);
            this.ckbFarolneblina.TabIndex = 24;
            this.ckbFarolneblina.Text = "Farol de Neblina";
            this.ckbFarolneblina.UseVisualStyleBackColor = true;
            // 
            // ckbVidroele
            // 
            this.ckbVidroele.AutoSize = true;
            this.ckbVidroele.Location = new System.Drawing.Point(6, 65);
            this.ckbVidroele.Name = "ckbVidroele";
            this.ckbVidroele.Size = new System.Drawing.Size(146, 17);
            this.ckbVidroele.TabIndex = 16;
            this.ckbVidroele.Text = "Vidros e Travas Elétricas ";
            this.ckbVidroele.UseVisualStyleBackColor = true;
            // 
            // ckbArCondicionado
            // 
            this.ckbArCondicionado.AutoSize = true;
            this.ckbArCondicionado.Location = new System.Drawing.Point(6, 42);
            this.ckbArCondicionado.Name = "ckbArCondicionado";
            this.ckbArCondicionado.Size = new System.Drawing.Size(104, 17);
            this.ckbArCondicionado.TabIndex = 15;
            this.ckbArCondicionado.Text = "Ar-Condicionado";
            this.ckbArCondicionado.UseVisualStyleBackColor = true;
            // 
            // ckbSensor
            // 
            this.ckbSensor.AutoSize = true;
            this.ckbSensor.Location = new System.Drawing.Point(6, 19);
            this.ckbSensor.Name = "ckbSensor";
            this.ckbSensor.Size = new System.Drawing.Size(153, 17);
            this.ckbSensor.TabIndex = 14;
            this.ckbSensor.Text = "Sensor de Estacionamento";
            this.ckbSensor.UseVisualStyleBackColor = true;
            // 
            // ckbOutros
            // 
            this.ckbOutros.AutoSize = true;
            this.ckbOutros.Location = new System.Drawing.Point(425, 65);
            this.ckbOutros.Name = "ckbOutros";
            this.ckbOutros.Size = new System.Drawing.Size(113, 17);
            this.ckbOutros.TabIndex = 25;
            this.ckbOutros.Text = "Outros acessórios:";
            this.ckbOutros.UseVisualStyleBackColor = true;
            // 
            // txtOutros
            // 
            this.txtOutros.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutros.Location = new System.Drawing.Point(425, 88);
            this.txtOutros.MaxLength = 50;
            this.txtOutros.Name = "txtOutros";
            this.txtOutros.Size = new System.Drawing.Size(129, 20);
            this.txtOutros.TabIndex = 26;
            // 
            // frmAutomovel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(643, 745);
            this.Controls.Add(this.gpb);
            this.Controls.Add(this.bntVoltar);
            this.Controls.Add(this.bntPesquisar);
            this.Controls.Add(this.bntAlterar);
            this.Controls.Add(this.bntExcluir);
            this.Controls.Add(this.bntCadastrar);
            this.Controls.Add(this.lblObrigatorio);
            this.Controls.Add(this.dtgautomovel);
            this.Controls.Add(this.cmbFornecedor);
            this.Controls.Add(this.txtcodigo);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtpreco);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txttransmissao);
            this.Controls.Add(this.txtcombustivel);
            this.Controls.Add(this.txtquilometragem);
            this.Controls.Add(this.txtversao);
            this.Controls.Add(this.txtcambio);
            this.Controls.Add(this.txtmotor);
            this.Controls.Add(this.txtportas);
            this.Controls.Add(this.txtcor);
            this.Controls.Add(this.txtano);
            this.Controls.Add(this.txtmodelo);
            this.Controls.Add(this.txtchassi);
            this.Controls.Add(this.lblcombustivel);
            this.Controls.Add(this.lblcambio);
            this.Controls.Add(this.lblversao);
            this.Controls.Add(this.lblportas);
            this.Controls.Add(this.lblmotor);
            this.Controls.Add(this.lblkm);
            this.Controls.Add(this.lbltransm);
            this.Controls.Add(this.lblano);
            this.Controls.Add(this.lblchassi);
            this.Controls.Add(this.lblcor);
            this.Controls.Add(this.lblmodelo);
            this.Controls.Add(this.pictureBoxCabecalho);
            this.MaximizeBox = false;
            this.Name = "frmAutomovel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Automovel";
            this.Load += new System.EventHandler(this.frmAutomovel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCabecalho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVoltar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntPesquisar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAlterar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntExcluir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCadastrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgautomovel)).EndInit();
            this.gpb.ResumeLayout(false);
            this.gpb.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxCabecalho;
        private System.Windows.Forms.ComboBox cmbFornecedor;
        private System.Windows.Forms.TextBox txtcodigo;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtpreco;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txttransmissao;
        private System.Windows.Forms.TextBox txtcombustivel;
        private System.Windows.Forms.TextBox txtquilometragem;
        private System.Windows.Forms.TextBox txtversao;
        private System.Windows.Forms.TextBox txtcambio;
        private System.Windows.Forms.TextBox txtmotor;
        private System.Windows.Forms.TextBox txtportas;
        private System.Windows.Forms.TextBox txtcor;
        private System.Windows.Forms.TextBox txtano;
        private System.Windows.Forms.TextBox txtmodelo;
        private System.Windows.Forms.TextBox txtchassi;
        private System.Windows.Forms.Label lblcombustivel;
        private System.Windows.Forms.Label lblcambio;
        private System.Windows.Forms.Label lblversao;
        private System.Windows.Forms.Label lblportas;
        private System.Windows.Forms.Label lblmotor;
        private System.Windows.Forms.Label lblkm;
        private System.Windows.Forms.Label lbltransm;
        private System.Windows.Forms.Label lblano;
        private System.Windows.Forms.Label lblchassi;
        private System.Windows.Forms.Label lblcor;
        private System.Windows.Forms.Label lblmodelo;
        private System.Windows.Forms.PictureBox bntVoltar;
        private System.Windows.Forms.PictureBox bntPesquisar;
        private System.Windows.Forms.PictureBox bntAlterar;
        private System.Windows.Forms.PictureBox bntExcluir;
        private System.Windows.Forms.PictureBox bntCadastrar;
        private System.Windows.Forms.Label lblObrigatorio;
        private System.Windows.Forms.DataGridView dtgautomovel;
        private System.Windows.Forms.GroupBox gpb;
        private System.Windows.Forms.CheckBox ckbVidroele;
        private System.Windows.Forms.CheckBox ckbArCondicionado;
        private System.Windows.Forms.CheckBox ckbSensor;
        private System.Windows.Forms.CheckBox ckbFarolneblina;
        private System.Windows.Forms.CheckBox ckbAdesivo;
        private System.Windows.Forms.CheckBox ckbOrganizador;
        private System.Windows.Forms.CheckBox ckbRadio;
        private System.Windows.Forms.CheckBox ckbGps;
        private System.Windows.Forms.CheckBox ckbMultimidia;
        private System.Windows.Forms.CheckBox ckbRoda;
        private System.Windows.Forms.CheckBox ckbBanco;
        private System.Windows.Forms.CheckBox ckbOutros;
        private System.Windows.Forms.TextBox txtOutros;
    }
}