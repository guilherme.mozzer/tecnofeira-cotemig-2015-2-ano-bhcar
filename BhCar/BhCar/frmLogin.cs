﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace BhCar
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        dadosdelogin dados = new dadosdelogin();
        private void bntCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblCriarUsuario_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txtUsuario.Clear();
            txtSenha.Clear();
            frmUsuário tela = new frmUsuário();
            tela.ShowDialog();
        }


        private void bnttExtrar_Click(object sender, EventArgs e)
        {
            dadosdelogin.Usuario = txtUsuario.Text;
            dadosdelogin.Nivel = cmbNivel.Text;

            string query = "SELECT login.usuario, login.senha, nivel.descricao FROM login JOIN nivel ON nivel.codigo = login.nivel_codigo WHERE login.usuario = @usuario AND login.senha = @senha AND nivel.descricao = @nivel";
            conexaobd conexao = new conexaobd();
            conexao.ConectarBD();

            MySqlCommand cmd = new MySqlCommand(query, conexao.con);

            cmd.Parameters.AddWithValue("@usuario", txtUsuario.Text);
            cmd.Parameters.AddWithValue("@senha", txtSenha.Text);
            cmd.Parameters.AddWithValue("@nivel", cmbNivel.Text);
            MySqlDataReader dr = cmd.ExecuteReader();

            if (dr.Read())
            {
                txtUsuario.Clear();
                txtSenha.Clear();
                cmbNivel.Text = "";
                frmMenu telam = new frmMenu();
                telam.ShowDialog();
            }
            else
            {
                MessageBox.Show("Usuário e/ou Senha e/ou nível Incorretos!", "Aviso",
            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtSenha.Clear();
                cmbNivel.Text = "";
                txtSenha.Focus();

            }
        }
       }
}

