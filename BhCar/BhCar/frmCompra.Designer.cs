﻿namespace BhCar
{
    partial class frmCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCompra));
            this.pictureBoxCabecalho = new System.Windows.Forms.PictureBox();
            this.bntVoltar = new System.Windows.Forms.PictureBox();
            this.bntPesquisar = new System.Windows.Forms.PictureBox();
            this.bntAlterar = new System.Windows.Forms.PictureBox();
            this.bntExcluir = new System.Windows.Forms.PictureBox();
            this.bntCadastrar = new System.Windows.Forms.PictureBox();
            this.lblObrigatorio = new System.Windows.Forms.Label();
            this.dtgCompra = new System.Windows.Forms.DataGridView();
            this.cmbPagamento = new System.Windows.Forms.ComboBox();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPreco = new System.Windows.Forms.TextBox();
            this.lblPreco = new System.Windows.Forms.Label();
            this.cmbvendedor = new System.Windows.Forms.ComboBox();
            this.lblVendedor = new System.Windows.Forms.Label();
            this.cmbautomovel = new System.Windows.Forms.ComboBox();
            this.cmbcliente = new System.Windows.Forms.ComboBox();
            this.lblForma = new System.Windows.Forms.Label();
            this.lblAutomóvel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDesconto = new System.Windows.Forms.TextBox();
            this.lblDesconto = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.txtQuantParcelas = new System.Windows.Forms.TextBox();
            this.lblParcela = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCabecalho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVoltar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntPesquisar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAlterar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntExcluir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCadastrar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCompra)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxCabecalho
            // 
            this.pictureBoxCabecalho.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxCabecalho.Image")));
            this.pictureBoxCabecalho.Location = new System.Drawing.Point(0, 1);
            this.pictureBoxCabecalho.Name = "pictureBoxCabecalho";
            this.pictureBoxCabecalho.Size = new System.Drawing.Size(673, 74);
            this.pictureBoxCabecalho.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCabecalho.TabIndex = 4;
            this.pictureBoxCabecalho.TabStop = false;
            // 
            // bntVoltar
            // 
            this.bntVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntVoltar.Image = ((System.Drawing.Image)(resources.GetObject("bntVoltar.Image")));
            this.bntVoltar.Location = new System.Drawing.Point(490, 497);
            this.bntVoltar.Name = "bntVoltar";
            this.bntVoltar.Size = new System.Drawing.Size(150, 40);
            this.bntVoltar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntVoltar.TabIndex = 242;
            this.bntVoltar.TabStop = false;
            this.bntVoltar.Click += new System.EventHandler(this.bntVoltar_Click);
            // 
            // bntPesquisar
            // 
            this.bntPesquisar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("bntPesquisar.Image")));
            this.bntPesquisar.Location = new System.Drawing.Point(178, 451);
            this.bntPesquisar.Name = "bntPesquisar";
            this.bntPesquisar.Size = new System.Drawing.Size(150, 40);
            this.bntPesquisar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntPesquisar.TabIndex = 241;
            this.bntPesquisar.TabStop = false;
            this.bntPesquisar.Click += new System.EventHandler(this.bntPesquisar_Click);
            // 
            // bntAlterar
            // 
            this.bntAlterar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntAlterar.Image = ((System.Drawing.Image)(resources.GetObject("bntAlterar.Image")));
            this.bntAlterar.Location = new System.Drawing.Point(334, 451);
            this.bntAlterar.Name = "bntAlterar";
            this.bntAlterar.Size = new System.Drawing.Size(150, 40);
            this.bntAlterar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntAlterar.TabIndex = 240;
            this.bntAlterar.TabStop = false;
            this.bntAlterar.Click += new System.EventHandler(this.bntAlterar_Click);
            // 
            // bntExcluir
            // 
            this.bntExcluir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntExcluir.Image = ((System.Drawing.Image)(resources.GetObject("bntExcluir.Image")));
            this.bntExcluir.Location = new System.Drawing.Point(490, 451);
            this.bntExcluir.Name = "bntExcluir";
            this.bntExcluir.Size = new System.Drawing.Size(150, 40);
            this.bntExcluir.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntExcluir.TabIndex = 239;
            this.bntExcluir.TabStop = false;
            this.bntExcluir.Click += new System.EventHandler(this.bntExcluir_Click);
            // 
            // bntCadastrar
            // 
            this.bntCadastrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntCadastrar.Image = ((System.Drawing.Image)(resources.GetObject("bntCadastrar.Image")));
            this.bntCadastrar.Location = new System.Drawing.Point(22, 451);
            this.bntCadastrar.Name = "bntCadastrar";
            this.bntCadastrar.Size = new System.Drawing.Size(150, 40);
            this.bntCadastrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bntCadastrar.TabIndex = 238;
            this.bntCadastrar.TabStop = false;
            this.bntCadastrar.Click += new System.EventHandler(this.bntCadastrar_Click);
            // 
            // lblObrigatorio
            // 
            this.lblObrigatorio.AutoSize = true;
            this.lblObrigatorio.Location = new System.Drawing.Point(524, 219);
            this.lblObrigatorio.Name = "lblObrigatorio";
            this.lblObrigatorio.Size = new System.Drawing.Size(111, 13);
            this.lblObrigatorio.TabIndex = 237;
            this.lblObrigatorio.Text = "* Campos Obrigatórios";
            // 
            // dtgCompra
            // 
            this.dtgCompra.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dtgCompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgCompra.Location = new System.Drawing.Point(22, 235);
            this.dtgCompra.Name = "dtgCompra";
            this.dtgCompra.Size = new System.Drawing.Size(618, 210);
            this.dtgCompra.TabIndex = 9;
            this.dtgCompra.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgCompra_CellContentClick);
            // 
            // cmbPagamento
            // 
            this.cmbPagamento.FormattingEnabled = true;
            this.cmbPagamento.Location = new System.Drawing.Point(492, 161);
            this.cmbPagamento.Name = "cmbPagamento";
            this.cmbPagamento.Size = new System.Drawing.Size(166, 21);
            this.cmbPagamento.TabIndex = 7;
            // 
            // txtCodigo
            // 
            this.txtCodigo.Enabled = false;
            this.txtCodigo.Location = new System.Drawing.Point(91, 81);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(255, 20);
            this.txtCodigo.TabIndex = 0;
            this.txtCodigo.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(39, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 252;
            this.label6.Text = "Código :";
            // 
            // txtPreco
            // 
            this.txtPreco.Location = new System.Drawing.Point(421, 135);
            this.txtPreco.Name = "txtPreco";
            this.txtPreco.Size = new System.Drawing.Size(237, 20);
            this.txtPreco.TabIndex = 5;
            this.txtPreco.TabStop = false;
            // 
            // lblPreco
            // 
            this.lblPreco.AutoSize = true;
            this.lblPreco.Location = new System.Drawing.Point(370, 138);
            this.lblPreco.Name = "lblPreco";
            this.lblPreco.Size = new System.Drawing.Size(45, 13);
            this.lblPreco.TabIndex = 250;
            this.lblPreco.Text = "* Preço:";
            // 
            // cmbvendedor
            // 
            this.cmbvendedor.FormattingEnabled = true;
            this.cmbvendedor.Location = new System.Drawing.Point(91, 107);
            this.cmbvendedor.Name = "cmbvendedor";
            this.cmbvendedor.Size = new System.Drawing.Size(255, 21);
            this.cmbvendedor.TabIndex = 2;
            // 
            // lblVendedor
            // 
            this.lblVendedor.AutoSize = true;
            this.lblVendedor.Location = new System.Drawing.Point(19, 110);
            this.lblVendedor.Name = "lblVendedor";
            this.lblVendedor.Size = new System.Drawing.Size(66, 13);
            this.lblVendedor.TabIndex = 248;
            this.lblVendedor.Text = "* Vendedor :";
            // 
            // cmbautomovel
            // 
            this.cmbautomovel.FormattingEnabled = true;
            this.cmbautomovel.Location = new System.Drawing.Point(91, 134);
            this.cmbautomovel.Name = "cmbautomovel";
            this.cmbautomovel.Size = new System.Drawing.Size(255, 21);
            this.cmbautomovel.TabIndex = 4;
            this.cmbautomovel.SelectedIndexChanged += new System.EventHandler(this.cmbautomovel_SelectedIndexChanged);
            // 
            // cmbcliente
            // 
            this.cmbcliente.FormattingEnabled = true;
            this.cmbcliente.Location = new System.Drawing.Point(421, 81);
            this.cmbcliente.Name = "cmbcliente";
            this.cmbcliente.Size = new System.Drawing.Size(237, 21);
            this.cmbcliente.TabIndex = 1;
            // 
            // lblForma
            // 
            this.lblForma.AutoSize = true;
            this.lblForma.Location = new System.Drawing.Point(362, 164);
            this.lblForma.Name = "lblForma";
            this.lblForma.Size = new System.Drawing.Size(126, 13);
            this.lblForma.TabIndex = 245;
            this.lblForma.Text = "* Formas de Pagamento :";
            // 
            // lblAutomóvel
            // 
            this.lblAutomóvel.AutoSize = true;
            this.lblAutomóvel.Location = new System.Drawing.Point(15, 137);
            this.lblAutomóvel.Name = "lblAutomóvel";
            this.lblAutomóvel.Size = new System.Drawing.Size(70, 13);
            this.lblAutomóvel.TabIndex = 244;
            this.lblAutomóvel.Text = "* Automóvel :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(373, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 243;
            this.label1.Text = "Cliente:";
            // 
            // txtDesconto
            // 
            this.txtDesconto.Location = new System.Drawing.Point(150, 187);
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.Size = new System.Drawing.Size(196, 20);
            this.txtDesconto.TabIndex = 8;
            // 
            // lblDesconto
            // 
            this.lblDesconto.AutoSize = true;
            this.lblDesconto.Location = new System.Drawing.Point(81, 190);
            this.lblDesconto.Name = "lblDesconto";
            this.lblDesconto.Size = new System.Drawing.Size(63, 13);
            this.lblDesconto.TabIndex = 255;
            this.lblDesconto.Text = "* Desconto:";
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Location = new System.Drawing.Point(373, 110);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(97, 13);
            this.lblData.TabIndex = 257;
            this.lblData.Text = "* Data de Compra :";
            // 
            // txtQuantParcelas
            // 
            this.txtQuantParcelas.Location = new System.Drawing.Point(150, 161);
            this.txtQuantParcelas.Name = "txtQuantParcelas";
            this.txtQuantParcelas.Size = new System.Drawing.Size(196, 20);
            this.txtQuantParcelas.TabIndex = 6;
            // 
            // lblParcela
            // 
            this.lblParcela.AutoSize = true;
            this.lblParcela.Location = new System.Drawing.Point(17, 164);
            this.lblParcela.Name = "lblParcela";
            this.lblParcela.Size = new System.Drawing.Size(134, 13);
            this.lblParcela.TabIndex = 259;
            this.lblParcela.Text = "* Quantidade de Parcelas :";
            // 
            // txtData
            // 
            this.txtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtData.Location = new System.Drawing.Point(476, 108);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(182, 20);
            this.txtData.TabIndex = 3;
            // 
            // frmCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 558);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.txtQuantParcelas);
            this.Controls.Add(this.lblParcela);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.txtDesconto);
            this.Controls.Add(this.lblDesconto);
            this.Controls.Add(this.cmbPagamento);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtPreco);
            this.Controls.Add(this.lblPreco);
            this.Controls.Add(this.cmbvendedor);
            this.Controls.Add(this.lblVendedor);
            this.Controls.Add(this.cmbautomovel);
            this.Controls.Add(this.cmbcliente);
            this.Controls.Add(this.lblForma);
            this.Controls.Add(this.lblAutomóvel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bntVoltar);
            this.Controls.Add(this.bntPesquisar);
            this.Controls.Add(this.bntAlterar);
            this.Controls.Add(this.bntExcluir);
            this.Controls.Add(this.bntCadastrar);
            this.Controls.Add(this.lblObrigatorio);
            this.Controls.Add(this.dtgCompra);
            this.Controls.Add(this.pictureBoxCabecalho);
            this.MaximizeBox = false;
            this.Name = "frmCompra";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vendas";
            this.Load += new System.EventHandler(this.frmCompra_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCabecalho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntVoltar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntPesquisar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntAlterar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntExcluir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bntCadastrar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgCompra)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxCabecalho;
        private System.Windows.Forms.PictureBox bntVoltar;
        private System.Windows.Forms.PictureBox bntPesquisar;
        private System.Windows.Forms.PictureBox bntAlterar;
        private System.Windows.Forms.PictureBox bntExcluir;
        private System.Windows.Forms.PictureBox bntCadastrar;
        private System.Windows.Forms.Label lblObrigatorio;
        private System.Windows.Forms.DataGridView dtgCompra;
        private System.Windows.Forms.ComboBox cmbPagamento;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtPreco;
        private System.Windows.Forms.Label lblPreco;
        private System.Windows.Forms.ComboBox cmbvendedor;
        private System.Windows.Forms.Label lblVendedor;
        private System.Windows.Forms.ComboBox cmbautomovel;
        private System.Windows.Forms.ComboBox cmbcliente;
        private System.Windows.Forms.Label lblForma;
        private System.Windows.Forms.Label lblAutomóvel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDesconto;
        private System.Windows.Forms.Label lblDesconto;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.TextBox txtQuantParcelas;
        private System.Windows.Forms.Label lblParcela;
        private System.Windows.Forms.DateTimePicker txtData;
    }
}